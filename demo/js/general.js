$(window).scrollTop(0);
$(document).ready(function(){
	$(window).scroll(function(){
		var header = $('.header'),
		scroll = $(window).scrollTop();
		if (scroll >=1) header.addClass('scroll-fix');
		else header.removeClass('scroll-fix');
	});
	$('.circle').bind('click',function(){
		$('html,body').animate({
			scrollTop:640
			},'4000');
	});
	$('.menu-icon').bind('click',function(){
			$('.menu-icon').toggleClass("menu-active");
			$('.navbar').fadeToggle();
	   });
	//$('.contact-title, .contact-text').css({'transform':'translateY(0px)','opacity':'1','pointer-events':'auto',});
	$('.thumbnail-common').bind('click',function(e){
		$('.lightbox').addClass('lightbox-visible');
		var title = $(this).attr('data-title');
		var subtitle=$(this).attr('data-subtitle');
		var src = $(this).attr('data-image-url');
		var description = $(this).attr('data-description');
		$('.data-title span').text(title);
		$('.data-subtitle span').text(subtitle);
		$('.data-image').attr('src',src);
		$('.desc-c').hide();
		$('#'+description).show();
		e.stopPropagation();
	});
	$('.lightbox').bind('click',function(){
		console.log('lightbox');
		$('.lightbox').removeClass('lightbox-visible');
	});
	$('.close-icon span').bind('click',function(){
		console.log('lightbox');
		$('.lightbox').removeClass('lightbox-visible');
	});
	$('.l-model').bind('click',function(e){
		e.stopPropagation();
		console.log('modal');			
	});
});
(function ($) {

})( jQuery );




/**
* The Animation object holds all of the selectors and animation functions
* for each item that needs to be animated
* 
* @class Animation
* 
* @param containerID {String} jQuery selector for the container
* @param options {Object} all of the items and styles you want to animate
*/
function ParallaxAnimation (containerID, options, state) {

	// Initiate variables
	var viewport = jQuery(window).height(),
		container = jQuery(containerID);
	var containerPos = container.offset().top,
		selectors = {},
		selector,
		styles,
		style;

	// Store for reCalc() on resize
	this.containerID = containerID;
	this.options = options;

	// Assign public properties
	this.start = (options.start) ? options.start : 0.5;
	this.stop = (options.stop) ? options.stop : this.start;
	this.state = state || 'start';
	this.calcStart = containerPos - (viewport * this.start);
	this.calcStop = (containerPos + container.height()) - (viewport * this.stop);
	this.startPercent = (this.calcStop - this.calcStart);
	this.itemNames = [];
	this.items = {};

	// Store each selector and what styles need to be animated
	for (selector in options) {
		if (selector !== 'start' || selector !== 'stop') {
			styles = options[selector];
			this.items[selector] = this.items[selector] ? this.items[selector] : {};
			this.items[selector].elem = jQuery(selector);
			this.items[selector].styles = [];
			this.itemNames.push(selector);
			for (style in styles) {
				// range, format, multiplier
				var item = this.items[selector],
				    tempStyles,
				    range = {
				    	start: (typeof styles[style][0] === 'function') ? styles[style][0]() : styles[style][0],
				    	stop: (typeof styles[style][1] === 'function') ? styles[style][1]() : styles[style][1]
				    };
				tempStyles = {};
				tempStyles.style = style;
				tempStyles.range = [range.start, range.stop];
				tempStyles.format = styles[style][2] || '';
				tempStyles.ease = styles[style][3] || 'linear';
				tempStyles.multiplier = multiplier(tempStyles.range);
				item.styles.push(tempStyles);
			}
		}
	}

	// Multiplier
	function multiplier(range) {
		var a = range[0],
			b = range[1],
			multiplier = Math.abs(a - b);

		if (a > b) {
			return function (percent) {
				return a - (percent * multiplier);
			};
		} else {
			return function (percent) {
				return a + (percent * multiplier);
			};
		}
	}

	/**
	* Get the current percent completed of this animation
	* 
	* @method getPercent
	* 
	* @param scroll {Number} current window scroll position
	*/
	this.getPercent = function (scroll) {
		return (scroll - this.calcStart) / this.startPercent;
	}
};

/**
* Animate function to progress the animation based on percent
* 
* @method animate
* 
* @param scroll {Number} The current window scroll position
* 
* @param status {String} The Status of 'start' or 'stop'
*/
ParallaxAnimation.prototype.animate = function (scroll, status) {
	var len = this.itemNames.length,
		i, j, slen,
		percent,
		item,
		change,
		current;

	for (i = 0; i < len; i++) {
		percent = this.getPercent(scroll);
		// Select current item
		item = this.items[this.itemNames[i]];
		slen = item.styles.length;
		// run multiplier on each style
		for (j = 0; j < slen; j++) {
			/**
			*
			*	Needs to be refactored
			*
			*/
			current = item.styles[j];
			if (current.ease) {
				change = current.multiplier(this.ease[current.ease](percent));
			} else {
				change = current.multiplier(percent);
			}

			if (current.style === 'rotate') {
				if (status !== 'stop') {
					item.elem.rotate(change);
				}
			} else {
				if (status && current.style === 'opacity') {
					item.elem.css(current.style, '');
					if (current.multiplier(percent) <= 0) {
						item.elem.hide();
					} else {
						item.elem.show();
					}
				} else {
					if (current.style === 'opacity') {
						item.elem.show();	
					}
					item.elem.css(current.style, change + current.format);
				}
			}
			/**
			*
			*	End of refactoring
			*
			*/
		}
	}
};


/**
* Easing functions for use in the animate method
* 
* @property ease
* 
* @type Object
*/
ParallaxAnimation.prototype.ease = {
	// ToDo
	ellipse: function (p) {
		return p * p * p * p;
	},
	circle: function (p) {
		return 1 - Math.sqrt(1 - (p * p));
	},
	begin: function() {
		return 0;
	},
	end: function() {
		return 1;
	},
	linear: function(p) {
		return p;
	},
	quadratic: function(p) {
		return p * p;
	},
	cubic: function(p) {
		return p * p * p;
	},
	swing: function(p) {
		return (-Math.cos(p * Math.PI) / 2) + 0.5;
	},
	sqrt: function(p) {
		return Math.sqrt(p);
	},
	outCubic: function(p) {
		return (Math.pow((p - 1), 3) + 1);
	},
	outQuad: function (p) {
		return p * (2 - p);
	},
	//see https://www.desmos.com/calculator/tbr20s8vd2 for how I did this
	bounce: function(p) {
		var a;

		if(p <= 0.5083) {
			a = 3;
		} else if(p <= 0.8489) {
			a = 9;
		} else if(p <= 0.96208) {
			a = 27;
		} else if(p <= 0.99981) {
			a = 91;
		} else {
			return 1;
		}

		return 1 - Math.abs(3 * Math.cos(p * a * 1.028) / a);
	}
}


var timeList = [];


/**
* Parallax takes an input of elements an options to create a
* parallax scroll effect
* 
* @class Parallax
* 
* @param options {Object} yes, all your options in one object
*/
function Parallax (options, state) {
	// If not called as a constructor recalls as constuctor
	if (this === undefined || this.constructor !== Parallax) {
		console.log('not being constructed');
		return new Parallax(options);
	}

	var $this = this;

	// Memoize each animation and make it public incase they want to remove one.
	this.animations = [];

	// init create each animation from options
	var key;
	for (key in options) {
		this.animations.push(new ParallaxAnimation(key, options[key], state));
	};

	// Setup for the scroll event listener
	var scrollCache = 0,
		theWindow = jQuery(window);

	jQuery(window).scroll(function () {

		scroll = theWindow.scrollTop();

		$this.run(scroll, scrollCache);

		scrollCache = scroll;
	});



	var stopper;

	jQuery(window).resize(function () {
		clearTimeout(stopper);
		stopper = setTimeout(function () {
			$this.reCalc();
		}, 100);
	});

};

/**
* Re-calculate the parallax numbers
* 
* @class Parallax
* 
* @method reCalc
*/
Parallax.prototype.reCalc = function () {
	var len = this.animations.length, i, item;
	for (i = 0; i < len; i++) {
		item = this.animations[i];
		this.animations[i] = new ParallaxAnimation(item.containerID, item.options);
	};
};



/**
* Run the animation for each animation in the animations array
* 
* @method run
* 
* @param scroll {Number} current scroll position in 'px'
* 
* @param $this {Object} It's the context...
*/
Parallax.prototype.run = function (scroll, scrollCache) {
	var len = this.animations.length, i, anim = this.animations;

	function runLoop (animations, i) {
		var current = animations[i];
		if (current.calcStart <= scroll && current.calcStop >= scroll) {
			current.state = 'running';
			current.animate(scroll);
		} else if (current.state !== 'start' && current.calcStart > scroll) {
			current.state = 'start';
			current.animate(current.calcStart, 'start');
		} else if (current.state !== 'stop' && current.calcStop < scroll) {
			current.state = 'stop';
			current.animate(current.calcStop, 'stop');
		}
	}

	if ((scroll - scrollCache) >= 0) {
		for (i = 0; i < len; i++) {
			runLoop(anim, i);
		}
	} else {
		for (i = len - 1; i >= 0; i--) {
			runLoop(anim, i);
		}
	}
}

/**
* Set the animation to done, like it has been scrolled to the bottom
* 
* @method setFinished
*/
Parallax.prototype.setFinished = function () {
	var len = this.animations.length, i, current;	
	for (i = 0; i < len; i++) {
		current = this.animations[i];
		current.animate(current.calcStop, 'stop');
	}
	this.animations = [];
};



$(window).scrollTop(0);
$(function(){
	$('.emp-5').addClass('emp-visible');
	$('.student_body_photo').bind('click',function(e){
		$('.emp-c').removeClass('emp-visible');
		var d_class = $(this).attr('data-class');
		$('.'+d_class).addClass('emp-visible');
		e.stopPropagation();
	});	
});

$(function(){
$('.menu-icon').bind('click',function(e){
			$('.menu-icon').toggleClass("menu-active");
			$('.navbar').fadeToggle();
			e.stopPropagation();
	   });
 });
 
$(function(){
	$('.google-search input').val("");
	$('.gsearch-btn').bind('click',function(e){
		var t1=$('.google-search input');
		console.log(t1.val());
		if(t1.val()!="")
		{
			$('.google-search').addClass('ginput-visible');
		}
		else		
		{
			$('.google-search').toggleClass('ginput-visible');
		}
		e.stopPropagation();
	});
});
// Image Slider JS

$(function(){

var image_container1 = $("#image_container1");
var image_container2 = $("#image_container2");
var image_container3 = $("#image_container3");
var image_container4 = $("#image_container4");
var image_container5 = $("#image_container5");


setTimeout(function(){
show_one();
},500);

function show_one(){
$('.image_container').removeClass('image_visible');
image_container1.addClass('image_visible');
$('.image_text_container').removeClass('text_visible');
$("#image_text_container1").addClass('text_visible');
setTimeout(function(){
show_two();
},4000);
}


function show_two(){
$('.image_container').removeClass('image_visible');
image_container2.addClass('image_visible');
$('.image_text_container').removeClass('text_visible');
$("#image_text_container2").addClass('text_visible');
setTimeout(function(){
show_three();
},4000);
}


function show_three(){
$('.image_container').removeClass('image_visible');
image_container3.addClass('image_visible');
$('.image_text_container').removeClass('text_visible');
$("#image_text_container3").addClass('text_visible');
setTimeout(function(){
show_four();
},4000);
}

function show_four(){
$('.image_container').removeClass('image_visible');
image_container4.addClass('image_visible');
$('.image_text_container').removeClass('text_visible');
$("#image_text_container4").addClass('text_visible');
setTimeout(function(){
show_five();
},4000);
}

function show_five(){
$('.image_container').removeClass('image_visible');
image_container5.addClass('image_visible');
$('.image_text_container').removeClass('text_visible');
$("#image_text_container5").addClass('text_visible');
setTimeout(function(){
show_one();
},4000);
}
});

// Custom DropDown JS


function DropDown(el) {
				this.dd = el;
				this.placeholder = this.dd.children('span');
				this.opts = this.dd.find('ul.dropdown > li');
				this.val = '';
				this.index = -1;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.bind('click', function(event){
						$(this).toggleClass('active');
						return false;
					});

					obj.opts.bind('click',function(){
						var opt = $(this);
						obj.val = opt.text();
						obj.index = opt.index();
						obj.placeholder.text(obj.val);
					});
				},
				getValue : function() {
					return this.val;
				},
				getIndex : function() {
					return this.index;
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd-1') );
				var dd1 = new DropDown( $('#dd-2') );
				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-3').toggleClass('active');
				});

			});
<!DOCTYPE html>
<html>
	<head>
		<title>
		BATCH ACTION
		</title>
			<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<div class="button-options">
					<a href="index.php">SUBMIT ARTICLE</a>
					<a href="app_id.php">APP ID</a>
					<a href="settings.php">SETTINGS</a>
					<a href="category.php">CATEGORIES</a>
					<a href="like_gate.php">LIKE GATE</a>
					<a href="#">ADVERTISEMENT</a>
					<a href="#">USERS</a>
					<a href="#">EXPORT</a>
					<a href="batch_post.php">BATCH POST</a>
					<a href="batch_action.php">BATCH ACTION</a>
					<a href="batch_notification.php">BATCH NOTIFICATION</a>
					<a href="logo.php">LOGO</a>
					<a href="footer.php">FOOTER</a>
				</div>
			</div>	
			<div class="form-container form-7" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
				<div class="batch-action-heading">
					<h2>Batch Action</h2>
				</div>
				<div class="">
					<input type="radio" class="" name="fradio-10"/>&nbsp;<strong>Post from group of urls (recommended)</strong><br>
				</div>
				<div class="batch-action-urls">
					<input type="text" name="batchurl11"/>
					<input type="text" name="batchurl12"/>
					<input type="text" name="batchurl13"/>
					<input type="text" name="batchurl14"/>
					<input type="text" name="batchurl15"/>
					<input type="text" name="batchurl16"/>
					<input type="text" name="batchurl17"/>
					<input type="text" name="batchurl18"/>
					<input type="text" name="batchurl19"/>
					<input type="text" name="batchurl20"/>
				</div>
				<div class="">
					<input type="radio" name="fradio-11" class=""/>&nbsp;<strong>Single url/photo/text</strong><br>
				</div>
				<div class="batch-inner-body">
					<div class="url">
						<label for="link-url">URL</label><br>
						<input type="text" name="furl" id="url"/>
					</div>
				</div>	
				<div class="batch-action-options margin-t10">
					<label>Number of users to notify at a time</label>&nbsp;&nbsp;<input type="text"  class="" value="1"/><br><br>
					<label>Time between actions in minutes </label>&nbsp;&nbsp;<input type="text"  class="" value="1"/>
				</div>
				<div class="save-btn">
					<input type="submit" name="fsave" value="SAVE"/>
				</div>
				<div>
					<label><h3>No Batch Actions </h3></label>
				</div>
			</div>
		
		</div>				
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/common-script.js" type="text/javascript"></script>
	</body>
</html>

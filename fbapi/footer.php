<!DOCTYPE html>
<html>
	<head>
		<title>
		FOOTER
		</title>
			<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<div class="button-options">
					<a href="index.php">SUBMIT ARTICLE</a>
					<a href="app_id.php">APP ID</a>
					<a href="settings.php">SETTINGS</a>
					<a href="category.php">CATEGORIES</a>
					<a href="like_gate.php">LIKE GATE</a>
					<a href="#">ADVERTISEMENT</a>
					<a href="#">USERS</a>
					<a href="#">EXPORT</a>
					<a href="batch_post.php">BATCH POST</a>
					<a href="batch_action.php">BATCH ACTION</a>
					<a href="batch_notification.php">BATCH NOTIFICATION</a>
					<a href="logo.php">LOGO</a>
					<a href="footer.php">FOOTER</a>
				</div>
			</div>	
			<div class="form-container form-10" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
				<div class="privacy">
					<h2>Privacy</h2>
					<textarea name="fprivacy" rows="5" cols="60">We only collect anonymous information.
					
We use technologies like cookies (small files stored by your browser), web beacons, or unique device identifiers to anonymously identify your computer or device so we can deliver a better experience. Our systems also log information like your browser, operating system and IP address.

We do not collect personally identifiable information about you. In other words, we do not collect information such as your name, address, phone number or email address.

We do not store or share your precise location.

We do not use or collect your precise geographic location.
					</textarea>
					<div class="save-btn">
						<input type="submit" name="fsave" value="SAVE"/>
					</div>
				</div>
				<div class="contact">
					<h2>Contact</h2>
					<textarea name="fcontact" rows="5" cols="60">your-email@domain.com</textarea>
					<div class="save-btn">
						<input type="submit" name="fsave" value="SAVE"/>
					</div>
				</div>
			</div>
		
		</div>				
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/common-script.js" type="text/javascript"></script>
	</body>
</html>

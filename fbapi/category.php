<!DOCTYPE html>
<html>
	<head>
		<title>
		Category
		</title>
			<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<div class="button-options">
					<a href="index.php">SUBMIT ARTICLE</a>
					<a href="app_id.php">APP ID</a>
					<a href="settings.php">SETTINGS</a>
					<a href="category.php">CATEGORIES</a>
					<a href="like_gate.php">LIKE GATE</a>
					<a href="#">ADVERTISEMENT</a>
					<a href="#">USERS</a>
					<a href="#">EXPORT</a>
					<a href="batch_post.php">BATCH POST</a>
					<a href="batch_action.php">BATCH ACTION</a>
					<a href="batch_notification.php">BATCH NOTIFICATION</a>
					<a href="logo.php">LOGO</a>
					<a href="footer.php">FOOTER</a>
				</div>
			</div>	
			<div class="form-container form-4" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
				<div class="category-head">
					<h2>Create Category</h2>
					<label for="cat-name">Name</label><br>
					<input type="text" name="fcatname" id="cat-name" value=""/>
					<div class="add-btn">
						<input type="submit" name="fadd" value="ADD"/>
					</div>
				</div>
				<div class="category-body">
					<table class="table">
						<thead>
							<tr>
								<th><input type="checkbox" name="fchkbox-3"/></th>
								<th><strong>Select all on this page </strong></th>
								<th> </th>
								<th> </th>
								<th> </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
								<input type="checkbox" name="fchkbox-3"/>
								</td>
								<td>Category Disabled</td>
								<td>In menu</td>
								<td>
								<a href="#">Edit</a>
								</td>
								<td>
								<a href="#">Delete</a>
								</td>
							</tr>
							<tr>
								<td>
								<input type="checkbox" name="fchkbox-3"/>
								</td>
								<td>Category Disabled</td>
								<td>In menu</td>
								<td>
								<a href="#">Edit</a>
								</td>
								<td>
								<a href="#">Delete</a>
								</td>
							</tr>
							<tr>
								<td>
								<input type="checkbox" name="fchkbox-3"/>
								</td>
								<td>Category Disabled</td>
								<td>In menu</td>
								<td>
								<a href="#">Edit</a>
								</td>
								<td>
								<a href="#">Delete</a>
								</td>
							</tr>
						</tbody>
					</table>			
					<div class="category-btn">
						<span>Selected:</span>&nbsp;&nbsp;
						<input type="submit" name="fdelcategory" value="Delete Categories"/>&nbsp;&nbsp;
						<input type="submit" name="faddtomenu" value="Add to menu"/>
					</div>
				</div>
			</div>
		
		</div>				
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/common-script.js" type="text/javascript"></script>
	</body>
</html>

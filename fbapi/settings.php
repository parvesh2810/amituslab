<!DOCTYPE html>
<html>
	<head>
		<title>
		Settings
		</title>
			<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<div class="button-options">
					<a href="index.php">SUBMIT ARTICLE</a>
					<a href="app_id.php">APP ID</a>
					<a href="settings.php">SETTINGS</a>
					<a href="category.php">CATEGORIES</a>
					<a href="like_gate.php">LIKE GATE</a>
					<a href="#">ADVERTISEMENT</a>
					<a href="#">USERS</a>
					<a href="#">EXPORT</a>
					<a href="batch_post.php">BATCH POST</a>
					<a href="batch_action.php">BATCH ACTION</a>
					<a href="batch_notification.php">BATCH NOTIFICATION</a>
					<a href="logo.php">LOGO</a>
					<a href="footer.php">FOOTER</a>
				</div>
			</div>	
			<div class="form-container form-3" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
				<div class="fb-app-auth">
					<label>Facebook app authorize</label><br>
					<input type="checkbox" class="checkbox" name="fchkbox-1"/>&nbsp;<span><strong>(Recommended)</strong> Activate Facebook authorize </span>
					<div class="save-btn">
						<input type="submit" name="fsave" value="SAVE"/>
					</div>
				</div>
				<div class="mobile-user">
					<label>Mobile users</label><br>
					<input type="checkbox" class="checkbox" name="fchkbox-2"/>&nbsp;<span><strong>(Recommended)</strong> Select this will increase traffic  </span>
					<div class="save-btn">
						<input type="submit" name="fsave" value="SAVE"/>
					</div>
				</div>
				<div class="use-as">
					<label>Use as</label><br>
					<input type="radio" class="checkbox" name="fradio-1"/>&nbsp;<span><strong>(Recommended)</strong> Website at demo-apps.net</span><br>
					<input type="radio" name="fradio-1"/>&nbsp;<span>Canvas app at apps.facebook.com/newsdemoapp</span>
					<div class="save-btn">
						<input type="submit" name="fsave" value="SAVE"/>
					</div>
				</div>
				<div class="fb-pub-like">
					<label>Publish Like Action</label><br>
					<input type="radio" class="checkbox" name="fradio-2"/>&nbsp;<span>Deactivate like action</span><br>
					<input type="radio" name="fradio-2"/>&nbsp;<span>Publish action on each</span>
					<input id="f-like-users" type="text" name="flikeusers" value=""/><span>users. If set 3, will publish 1 and will skip 3 users</span><br>
					<input type="radio" name="fradio-2"/>&nbsp;<span><strong>(Recommended)</strong> Publish one action per day for each user</span>
					<div class="save-btn">
						<input type="submit" name="fsave" value="SAVE"/>
					</div>
				</div>
				<div class="auto-share">
					<label>Auto Share</label><br>
					<input type="radio" class="checkbox" name="fradio-3"/>&nbsp;<span>Deactivate auto share</span><br>
					<input type="radio" name="fradio-3"/>&nbsp;<span><strong>(Recommended)</strong> Publish one action per day for each user</span><br>
					<input type="radio" name="fradio-3"/>&nbsp;<span>Auto share on each </span>
					<input id="auto-share-users" type="text" name="fautoshare" value=""/><span>users. If set 5, will share on 1 and will skip 5 users</span>
					
					<div class="save-btn">
						<input type="submit" name="fsave" value="SAVE"/>
					</div>
				</div>
				
			</div>
		
		</div>				
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/common-script.js" type="text/javascript"></script>
	</body>
</html>

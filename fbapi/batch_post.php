<!DOCTYPE html>
<html>
	<head>
		<title>
		BATCH POST
		</title>
			<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<div class="button-options">
					<a href="index.php">SUBMIT ARTICLE</a>
					<a href="app_id.php">APP ID</a>
					<a href="settings.php">SETTINGS</a>
					<a href="category.php">CATEGORIES</a>
					<a href="like_gate.php">LIKE GATE</a>
					<a href="#">ADVERTISEMENT</a>
					<a href="#">USERS</a>
					<a href="#">EXPORT</a>
					<a href="batch_post.php">BATCH POST</a>
					<a href="batch_action.php">BATCH ACTION</a>
					<a href="batch_notification.php">BATCH NOTIFICATION</a>
					<a href="logo.php">LOGO</a>
					<a href="footer.php">FOOTER</a>
				</div>
			</div>	
			<div class="form-container form-6" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
				<div class="batch-post-heading">
					<h2>Batch Posting</h2>
				</div>
				<div class="">
					<input type="radio" class="" name="fradio-4"/>&nbsp;<strong>Post from group of urls (recommended)</strong><br>
				</div>
				<div class="batch-post-urls">
					<p>Add one post url in each of the fields, will post random url to users </p>
					<input type="text" name="batchurl1"/>
					<input type="text" name="batchurl2"/>
					<input type="text" name="batchurl3"/>
					<input type="text" name="batchurl4"/>
					<input type="text" name="batchurl5"/>
					<input type="text" name="batchurl6"/>
					<input type="text" name="batchurl7"/>
					<input type="text" name="batchurl8"/>
					<input type="text" name="batchurl9"/>
					<input type="text" name="batchurl10"/>
				</div>
				<div class="">
					<input type="radio" name="fradio-5" class=""/>&nbsp;<strong>Single url/photo/text</strong><br>
				</div>
				<div class="batch-inner-body">
					<div class="message">
						<label for="message">Message</label><br>
						<input type="text" name="fmessage" id="message"/>
					</div>
					<div class="link-url">
						<label for="link-url">Link URL</label><br>
						<input type="text" name="flinkurl" id="link-url"/>
					</div>
					<div class="link-title">
						<label for="link-title">Link Title</label><br>
						<input type="text" name="flinktitle" id="link-title"/>
					</div>
					<div class="caption">
						<label for="caption">Caption</label><br>
						<input type="text" name="caption" id="caption"/>
					</div>
					<div class="photo-url">
						<label for="tilte">Thumbnail Photo URL</label><br>
						<input type="text" name="fphotourl" id="photo-url"/>
					</div>
					<div class="description">
						<label for="description">Description</label><br>
						<textarea name="fdescription" id="description"></textarea>
					</div>
				</div>	
				<div class="margin-t10">
					<label>Number of users to post at a time</label>&nbsp;&nbsp;<input type="radio" name="fradio-6" class=""/>&nbsp;&nbsp;<span>1</span><br><br>
					<label>Time between posting in minutes </label>&nbsp;&nbsp;<input type="radio" name="fradio-7" class=""/>&nbsp;&nbsp;<span>1</span><br>
				</div>
				<div class="margin-t10">
					<input type="radio" name="fradio-8" class=""/>&nbsp;&nbsp;<label>POST</label><br><br>
					<input type="radio" name="fradio-9" class=""/>&nbsp;&nbsp;<label>DELETE </label>
				</div>
				<div class="save-btn">
					<input type="submit" name="fsave" value="SAVE"/>
				</div>
				<div>
					<label><h3>No Batch Posts </h3></label>
				</div>
			</div>
		
		</div>				
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/common-script.js" type="text/javascript"></script>
	</body>
</html>

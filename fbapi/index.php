<!DOCTYPE html>
<html>
	<head>
		<title>
		Index
		</title>
			<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<div class="button-options">
					<a href="index.php">SUBMIT ARTICLE</a>
					<a href="app_id.php">APP ID</a>
					<a href="settings.php">SETTINGS</a>
					<a href="category.php">CATEGORIES</a>
					<a href="like_gate.php">LIKE GATE</a>
					<a href="#">ADVERTISEMENT</a>
					<a href="#">USERS</a>
					<a href="#">EXPORT</a>
					<a href="batch_post.php">BATCH POST</a>
					<a href="batch_action.php">BATCH ACTION</a>
					<a href="batch_notification.php">BATCH NOTIFICATION</a>
					<a href="logo.php">LOGO</a>
					<a href="footer.php">FOOTER</a>
				</div>
			</div>	
			<div class="form-container form-1" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
				<div class="browse-image">
					<label for="file">*Upload image for the Front page thumbnail </label>
					<input type="file" name="fimage" id="file"/>
				</div>
				<div class="title-name">
					<label for="tilte">*Title</label><br>
					<input type="text" name="ftitle" id="title"/>
				</div>
				<div class="fb-message">
					<label for="message">Message for Facebook share</label><br>
					<textarea name="fmessage" id="message"></textarea>
				</div>
				<div class="description">
					<label for="description">Short description for home page below thumbnail</label><br>
					<textarea name="fdescription" id="description"></textarea>
				</div>
				<div class="content">
					<label for="content">*Content</label><br>
					<textarea name="fcontent" id="content"></textarea>
				</div>
				<div class="post-time">
					<label for="time">Time to Post</label><br>
						<input type="text" name="ftime" id="time"/>
					<span>Leave blank to post now </span>
				</div>
				<div class="post-category">
					<label>Category</label><br>
						<input type="radio" name="fradio"/><span>Uncategorised</span>
						<input type="radio" name="fradio"/><span>NEWS</span>
						<input type="radio" name="fradio"/><span>PHOTO</span>
						<input type="radio" name="fradio"/><span>VIDEO</span>
				</div>
				<div class="homepage-chkbox">
					<input type="checkbox" name="fchkbox"/>&nbsp;&nbsp;<span>Show this News on Home page. Uncheck and will show on Category page only </span>
				</div>
				<div class="submit-btn">
					<input type="submit" name="fsubmit" value="SUBMIT"/>
				</div>
				<div class="guide-tips">
					<p>Tips how to create galleries similar to likes.com</p>
					<p>1 Upload image for home page thumbnail</p>
					<p>2 Add title</p>
					<p>3 Add photo description</p>
					<p>4 Upload the first photo from the gallery in the editor, you can upload from computer or from image url</p>
					<p>5 Select the CATEGORY</p>
					<p>6 Select Show this News on Home page</p>
					<p>7 Submit</p><br>
					<p>8 For each next photo from the gallery repeat the same except this:
						Select Uncategorised and Uncheck Show this News on Home page</p><br>
					<p>Recommended photo/video width is 730px </p>
				</div>
			</div>
		
		</div>				
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/common-script.js" type="text/javascript"></script>
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
		<title>
		APP
		</title>
			<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
	</head>
	<body>
		<div class="container">
			<div class="header">
				<div class="button-options">
					<a href="index.php">SUBMIT ARTICLE</a>
					<a href="app_id.php">APP ID</a>
					<a href="settings.php">SETTINGS</a>
					<a href="category.php">CATEGORIES</a>
					<a href="like_gate.php">LIKE GATE</a>
					<a href="#">ADVERTISEMENT</a>
					<a href="#">USERS</a>
					<a href="#">EXPORT</a>
					<a href="batch_post.php">BATCH POST</a>
					<a href="batch_action.php">BATCH ACTION</a>
					<a href="batch_notification.php">BATCH NOTIFICATION</a>
					<a href="logo.php">LOGO</a>
					<a href="footer.php">FOOTER</a>
				</div>
			</div>	
			<div class="form-container form-2" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
				<div class="app-heading">
					<span>Add new app/secret</span>
				</div>
				<div class="app-id">
					<label for="app-id">APP ID</label><br>
					<input type="text" name="fappid" id="app-id" value="740533979295866"/>
				</div>
				<div class="app-secret">
					<label for="app-secret">APP Secret</label><br>
					<input type="text" name="fappsecret" id="app-secret" value="eb52aa1b2f07d19715e3faf8025d328c"/>
				</div>
				<div class="save-btn">
					<input type="submit" name="fsave" value="SAVE"/>
				</div>
			</div>
		
		</div>				
		<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/common-script.js" type="text/javascript"></script>
	</body>
</html>

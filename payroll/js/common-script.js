
window.onload = function() {
   $('.header .right-slide').animate({left:"38%"},500);
   $('.header .left-slide').animate({right:"41%"},500);
}

$(document).ready(function(){
// All Container Hide JS

	$('.top-tab-hidden').hide();
	$('.my-details-container').show();
	$('#my_details').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-details-container').show();
		$('.hidden-1').hide();
		$('.personal-details').show();
		$('.my-details-navbar li').removeClass('navbar-active');
		$('#personal').addClass('navbar-active');
	});
	$('#my_salary').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-salary-container').show();
		$('.hidden-2').hide();
		$('.salary-details').show();
		$('.my-salary-navbar li').removeClass('navbar-active');
		$('#salary').addClass('navbar-active');
	});
	$('#my_investments').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-investments-container').show();
	});
	$('#my_attendance').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-attendance-container').show();
	});
	$('#my_leaves').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-leaves-container').show();
	});
	$('#my_workflow').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-workflow-container').show();
	});
	
// My Details JS	
	
	$('.hidden-1').hide();
	$('.personal-details').show();
	$('#personal').addClass('navbar-active');
	$('#personal').bind('click',function(){
		$('.hidden-1').hide();
		$('.personal-details').show();
		$('.my-details-navbar li').removeClass('navbar-active');
		$('#personal').addClass('navbar-active');
	});
	$('.next-btn').bind('click',function(){
		$('.hidden-1').hide();
		$('.professional-details').show();
		$('.my-details-navbar li').removeClass('navbar-active');
		$('#professional').addClass('navbar-active');
	});
	$('#professional').bind('click',function(){
		$('.hidden-1').hide();
		$('.professional-details').show();
		$('.my-details-navbar li').removeClass('navbar-active');
		$('#professional').addClass('navbar-active');
	});
	
// My Salary JS
	
	$('.hidden-2').hide();
	$('.salary-details').show();
	$('#salary').addClass('navbar-active');
	$('#salary').bind('click',function(){
		$('.hidden-2').hide();
		$('.salary-details').show();
		$('.my-salary-navbar li').removeClass('navbar-active');
		$('#salary').addClass('navbar-active');
	});
	$('#payslips').bind('click',function(){
		$('.hidden-2').hide();
		$('.payslips-details').show();
		$('.my-salary-navbar li').removeClass('navbar-active');
		$('#payslips').addClass('navbar-active');
	});
	$('#taxslips').bind('click',function(){
		$('.hidden-2').hide();
		$('.taxslips-details').show();
		$('.my-salary-navbar li').removeClass('navbar-active');
		$('#taxslips').addClass('navbar-active');
	});
	$('#ytd').bind('click',function(){
		$('.hidden-2').hide();
		$('.ytd-details').show();
		$('.my-salary-navbar li').removeClass('navbar-active');
		$('#ytd').addClass('navbar-active');
	});
	$('#form16s').bind('click',function(){
		$('.hidden-2').hide();
		$('.form16s-details').show();
		$('.my-salary-navbar li').removeClass('navbar-active');
		$('#form16s').addClass('navbar-active');
	});
//	My Investment page JS
	
	$('.hidden-3').hide();
	$('.previous-year').show();
	$('#invest_prev').addClass('navbar-active');
	$('#invest_prev').bind('click',function(){
		$('.hidden-3').hide();
		$('.previous-year').show();
		$('.my-investments-navbar li').removeClass('navbar-active');
		$('#invest_prev').addClass('navbar-active');
	});
	$('#invest_curr').bind('click',function(){
		$('.hidden-3').hide();
		$('.current-year').show();
		$('.my-investments-navbar li').removeClass('navbar-active');
		$('#invest_curr').addClass('navbar-active');
	});
//	My Attendance page JS
	
	$('.hidden-4').hide();
	$('.days-details').show();
	$('#dates').addClass('navbar-active');
	$('#dates').bind('click',function(){
		$('.hidden-4').hide();
		$('.days-details').show();
		$('.my-attendance-navbar li').removeClass('navbar-active');
		$('#dates').addClass('navbar-active');
	});	
	$('#att_reportees').bind('click',function(){
		$('.hidden-4').hide();
	    $('.att-reportees-details').show();
		$('.my-attendance-navbar li').removeClass('navbar-active');
		$('#att_reportees').addClass('navbar-active');
	});	
//	My Leaves page JS

	$('.hidden-5').hide();
	$('.leaves-details').show();
	$('#leaves').addClass('navbar-active');
	$('#leaves').bind('click',function(){
		$('.hidden-5').hide();
		$('.leaves-details').show();
		$('.my-leaves-navbar li').removeClass('navbar-active');
		$('#leaves').addClass('navbar-active');
	});	
	$('#leaves_reportees').bind('click',function(){
		$('.hidden-5').hide();
	    $('.leaves-reportees-details').show();
		$('.my-leaves-navbar li').removeClass('navbar-active');
		$('#leaves_reportees').addClass('navbar-active');
	});	
});

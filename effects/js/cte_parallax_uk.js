jQuery(document).ready(function () {
	var options = {
		'.step-one-spacing .part-one': {
			'.line-fills .all-lines' : {
				height: [ 0, 100, '%']
			}, // Dots
			'.the-dot .left-dot': {
				'margin-top': [ 0, 15, '%']
			},
			'.the-dot .right-dot': {
				'margin-top': [ 0, 15, '%']
			},
			'.the-dot .center-dot': {
				'margin-top': [ 0, 15, '%']
			}
		},
		'.step-one-spacing .part-two': {
			'.each-line .fill-right' : {
				width: [ 5, 27, '%', 'circle'],
				height: [ 0, 100, '%']
			},
			'.each-line .fill-left' : {
				width: [ 5, 27, '%', 'circle'],
				height: [ 0, 100, '%']
			},
			'.each-line .fill-center' : {
				height: [ 0, 40, '%']
			}, // Dots
			'.the-dot .left-dot': {
				'margin-top': [ 15, 38.3, '%'],
				'margin-left': [ 1.7, 24, '%', 'circle' ]
			},
			'.the-dot .right-dot': {
				'margin-top': [ 15, 38.3, '%'],
				'margin-left': [ 98.7, 76, '%', 'circle' ]
			},
			'.the-dot .center-dot': {
				'margin-top': [ 15, 25, '%']
			}
		},
		'.step-one-spacing .part-three': {
			'.each-line .fill-right' : {
				width: [ 27, 50, '%']
			},
			'.each-line .fill-left' : {
				width: [ 27, 50, '%']
			},
			'.each-line .fill-center' : {
				height: [ 40, 95, '%']
			}, // Dots
			'.the-dot .left-dot': {
				'margin-left': [ 24, 49.7, '%' ]
			},
			'.the-dot .right-dot': {
				'margin-left': [ 76, 49.7, '%' ]
			},
			'.the-dot .center-dot': {
				'margin-top': [ 25, 38.3, '%']
			},
			'.the-dot .left-dot, .the-dot .right-dot, .the-dot .center-dot': {
				opacity: [ 1, 0, '', 'begin' ]
			},
			'.the-dot .main-dot': {
				opacity: [ 0, 1, '', 'begin' ]
			}
		},
		'.step-two-spacing .part-one': {
			'.step-two .fill-curve': {
				height: [ 0, 5, '%']
			}, // the dot
			'.step-two .main-dot': {
				'margin-top': [ -0.5, 3.5, '%']
			},
			'.the-dot .main-dot': {
				opacity: [ 0, 1, '', 'end' ]
			},
			'.the-dot .left-dot, .the-dot .right-dot, .the-dot .center-dot': {
				opacity: [ 1, 0, '', 'end' ]
			}
		},
		'.step-two-spacing .part-two': {
			'.step-two .fill-curve': {
				width: [ 5, 15, '%', 'circle'],
				height: [ 5, 50, '%']
			}, // The Dot
			'.step-two .main-dot': {
				'margin-top': [ 3.5, 21.5, '%' ],
				'margin-left': [ 1.3, 15, '%', 'circle' ]
			}
		},
		'.step-two-spacing .part-three': {
			'.step-two .fill-curve': {
				width: [ 15, 81, '%']
			}, // The Dot
			'.step-two .main-dot': {
				'margin-left': [ 15, 82, '%']
			}
		},
		'.step-two-spacing .part-four': {
			'.step-two .fill-curve': {
				width: [ 80, 100, '%'],
				height: [ 50, 80, '%', 'circle']
			}, // The Dot
			'.step-two .main-dot': {
				'margin-left': [ 82, 98.7, '%'],
				'margin-top': [ 21.5, 38.5, '%', 'circle']
			}
		},
		'.step-two-spacing .part-five': {
			'.step-two .fill-curve': {
				height: [ 80, 100, '%' ]
			}, // The Dot
			'.step-two .main-dot': {
				'margin-top': [ 38.5, 44, '%']
			}
		},
		'.step-three-spacing .part-two': {
			'.step-three .fill-line': {
				height: [ 0, 26, '%' ]
			}, // The Dot
			'.step-three .main-dot': {
				'margin-top': [ 0, 9, '%']
			}
		},
		'.step-three-spacing .part-three': {
			'.step-three .fill-line': {
				height: [ 26, 57, '%' ],
				width: [ 1.5, 13, '%', 'circle' ]
			}, // The Dot
			'.step-three .main-dot': {
				'margin-top': [ 9, 20.5, '%'],
				'margin-left': [ 99.2, 86, '%', 'circle']
			}
		},
		'.step-three-spacing .part-four': {
			'.step-three .fill-line': {
				width: [ 13, 88, '%' ]
			}, // The Dot
			'.step-three .main-dot': {
				'margin-left': [ 86, 13, '%' ]
			}
		},
		'.step-three-spacing .part-five': {
			'.step-three .fill-line': {
				height: [ 57, 86, '%', 'circle' ],
				width: [ 88, 100, '%' ]
			}, // The Dot
			'.step-three .main-dot': {
				'margin-top': [ 20.5, 32, '%', 'circle' ],
				'margin-left': [ 13, 0.7, '%' ]
			}
		},
		'.step-three-spacing .part-six': {
			'.step-three .fill-line': {
				height: [ 86, 100, '%']
			}, // The Dot
			'.step-three .main-dot': {
				'margin-top': [ 32, 38, '%' ]
			}
		},
		'.step-four-spacing .part-two': {
			'.step-four .fill-line': {
				height: [ 0, 23, '%' ]
			}, // The Dot
			'.step-four .main-dot': {
				'margin-top': [ 0, 12, '%' ]
			}
		},
		'.step-four-spacing .part-three': {
			'.step-four .fill-line': {
				height: [ 23, 58, '%' ],
				width: [ 1.7, 18, '%', 'circle' ]
			}, // The Dot
			'.step-four .main-dot': {
				'margin-top': [ 12, 24.5, '%' ],
				'margin-left': [ 0.9, 18, '%', 'circle' ]
			}
		},
		'.step-four-spacing .part-four': {
			'.step-four .fill-line': {
				width: [ 18, 83, '%' ]
			}, // The Dot
			'.step-four .main-dot': {
				'margin-left': [ 18, 83, '%' ]
			}
		},
		'.step-four-spacing .part-five': {
			'.step-four .fill-line': {
				height: [ 58, 100, '%', 'circle' ],
				width: [ 83, 100, '%' ]
			}, // The Dot
			'.step-four .main-dot': {
				'margin-top': [ 24.5, 42, '%', 'circle' ],
				'margin-left': [ 83, 98.5, '%' ]
			}
		},
		'.step-four-spacing .part-six': {
			'.text-block-four .browser-2': {
				opacity: [ 0, 1, '' ]
			},
			'.text-block-four .bubble img': {
				height: [ 0, 181, 'px' ]
			}
		}
	}

	var parallax = new Parallax(options);

	// Checking of ie8 (hack)
	if (! + '\v1') {
		parallax.setFinished();
	}
});
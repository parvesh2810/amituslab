$(document).ready(function(){
	$('#category').bind('click',function(){
		$('#category-sub').slideToggle();
	});
	$('#event-type').bind('click',function(){
		$('#event-type-sub').slideToggle();
	});
	$('#date').bind('click',function(){
		$('#date-sub').slideToggle();
	});
	$('#price').bind('click',function(){
		$('#price-sub').slideToggle();
	});
	
	$('.contact-title, .contact-text').css('transform','translateY(0px)');
	
	//Search events and press enter js
	
	
		$('.search-icon').bind('click', function() {
			if($('.header_left_search_input').val()!="")
			{
				console.log('search icon clicked');
			}
			else 
			{
				alert("Search field is Empty");
			}
		});
		$('.header_left_search_input').bind('keypress', function(e) {
			if(e.keyCode==13){
				if($('.header_left_search_input').val()!="")
					console.log('enter pressed');
				else
					alert("Search field is Empty");
			}
		});
	
	$('.social-checkbox input[type="checkbox"]').bind('click',function(e){
			$('.social-inputs').slideToggle();
	});
	
	// Radio Private Listing Event
	
	$('.privacy-2').bind('click',function(e){
			$('.event-inputs').hide();
			$('.private-page-options').slideToggle();
	});
	$('.privacy-1').bind('click',function(e){
			$('.private-page-options').hide();
			$('.event-inputs').slideToggle();
	});
	
	// Tick and No Tick events
	$('.relavance-btn').addClass('tick');
	$('.filter_button').bind('click',function(e){
		var filter_name = $(this).attr('data-filter-name');
		$('.filter_button').removeClass('tick');
		if(filter_name == 'relevance')
		{
			$('.relavance-btn').addClass('tick');
		}
		else
		{
			$('.date-btn').addClass('tick');
		}
		e.stopPropagation();
	});
	
	$('.all-recommend').addClass('recommendation-icon');
	$('.all-recommendations button').bind('click',function(e){
		var event_filter = $(this).attr('data-event-filter');
		$('.all-recommendations button').removeClass('recommendation-icon');
		if(event_filter == 'all')
		{
			$('.no-events').hide();
			$('.events-list').show();
			$('.all-recommend').addClass('recommendation-icon');
		}
		else
		{
			$('.events-list').hide();
			$('.friends-icon').addClass('recommendation-icon');
			$('.no-events').show();
		}
	});
	
	//Light Box Effects TIPS
	$('.tips-click').bind('click',function(e){
		console.log('ssccs');
		tips_type=$(this).attr('data-tip-type');
		console.log(tips_type);
		if(tips_type=="tip1")
		{
			console.log('tip1');
			$('.lightbox').addClass('lightbox-visible');
			$('.tip-step-c').hide();
			$('.tip-step1').show();
		}
		if(tips_type=="tip2")
		{
			console.log('tip2');
			$('.lightbox').addClass('lightbox-visible');
			$('.tip-step-c').hide();
			$('.tip-step2').show();
		}
		if(tips_type=="tip3")
		{
			console.log('tip3');
			$('.lightbox').addClass('lightbox-visible');
			$('.tip-step-c').hide();
			$('.tip-step3').show();
		}
	});
	//Light Box Effects Sign up and Login and Forgot
	
	$('.header_right_button').bind('click',function(e){
		entry_type=$(this).attr('data-user');
		console.log(entry_type);
		if(entry_type=="login")
		{
			console.log('login');
			$('.lightbox').addClass('lightbox-visible');
			$('.signup-container, .forgot-container').hide();
			$('.login-container').show();
			$('.l-model').css('height','500');
		}
		else if(entry_type=="signup")
		{
			console.log('signup');
			$('.lightbox').addClass('lightbox-visible');
			$('.login-container, .forgot-container').hide();
			$('.signup-container').show();
			$('.l-model').css('height','500');
		}
		else {
			console.log('nothing');
		}
	});
	
	$('.c-swap').bind('click',function(e){
		c_swap=$(this).attr('data-user-swap');
		if(c_swap=="login")
		{
			$('.signup-container, .forgot-container').hide();
			$('.login-container').show();
			$('.l-model').css('height','500');
			
		}
		if(c_swap=="signup")
		{
			$('.login-container, .forgot-container').hide();
			$('.signup-container').show();
			$('.l-model').css('height','500');
		}
		if(c_swap=="forgot")
		{
			$('.login-container, .signup-container').hide();
			$('.forgot-container').show();
			$('.l-model').css('height','200');
			
		}
	});
	
	$('.lightbox').bind('click',function(){
		console.log('lightbox');
		$('.lightbox').removeClass('lightbox-visible');
	});
	$('.close-icon span').bind('click',function(){
		console.log('lightbox');
		$('.lightbox').removeClass('lightbox-visible');
	});
	$('.l-model').bind('click',function(e){
		e.stopPropagation();
		console.log('modal');			
	});
	
	// Custom DropDown JS


function DropDown(el) {
				this.dd = el;
				this.placeholder = this.dd.children('span');
				this.opts = this.dd.find('ul.dropdown > li');
				this.val = '';
				this.index = -1;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.bind('click', function(event){
						$(this).toggleClass('active');
						return false;
					});

					obj.opts.bind('click',function(){
						var opt = $(this);
						obj.val = opt.text();
						obj.index = opt.index();
						obj.placeholder.text(obj.val);
					});
				},
				getValue : function() {
					return this.val;
				},
				getIndex : function() {
					return this.index;
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd-1') );
				var dd = new DropDown( $('#dd-2') );
				var dd = new DropDown( $('#dd-3') );
				var dd = new DropDown( $('#dd-4') );
				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-3').toggleClass('active');
				});

			});
	
});

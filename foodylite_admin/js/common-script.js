
window.onload = function() {
   $('.header .right-slide').animate({left:"38%"},500);
   $('.header .left-slide').animate({right:"41%"},500);
}
	
$(document).ready(function(){	
// All Container Hide JS
	$('.top-tab-hidden').hide();
	$('.my-company-container').show();
	$('#my_company').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-company-container').show();
		$('.hidden').hide();
		$('.company-details').show();
		$('.my-company-navbar li').removeClass('navbar-active');
		$('#details').addClass('navbar-active');
	});
	$('#my_employee').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-employees-container').show();
		$('.hidden-1').hide();
		$('.employees-search').show();
		$('.my-employees-navbar li').removeClass('navbar-active');
		$('#search_emp').addClass('navbar-active');
		
	});
	$('#my_statutories').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-statutories-container').show();
		$('.hidden-2').hide();
		$('.statutories-pf-groups').show();
		$('.my-statutories-navbar li').removeClass('navbar-active');
		$('#pf_groups').addClass('navbar-active');
	});
	$('#my_payroll').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-payroll-container').show();
		$('.hidden-3').hide();
		$('.payroll-earning-heads').show();
		$('.my-payroll-navbar li').removeClass('navbar-active');
		$('#earning_heads').addClass('navbar-active');
	});
	$('#my_attendanceprocessor').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-attendanceprocessor-container').show();
	});
	$('#my_salaryprocessor').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-salaryprocessor-container').show();
	});
	$('#my_tools').bind('click',function(){
		$('.top-tab-hidden').hide();
		$('.my-tools-container').show();
	});
	

// My Company page JS
	$('.hidden').hide();
	$('.company-details').show();
	$('#details').addClass('navbar-active');
	$('#details').bind('click',function(){
		$('.hidden').hide();
		$('.company-details').show();
		$('.my-company-navbar li').removeClass('navbar-active');
		$('#details').addClass('navbar-active');
	});
	$('#branches').bind('click',function(){
		$('.hidden').hide();
		$('.company-branches').show();
		$('.my-company-navbar li').removeClass('navbar-active');
		$('#branches').addClass('navbar-active');
	});
	$('#designations').bind('click',function(){
		$('.hidden').hide();
		$('.company-designations').show();
		$('.my-company-navbar li').removeClass('navbar-active');
		$('#designations').addClass('navbar-active');
	});
	$('#departments').bind('click',function(){
		$('.hidden').hide();
		$('.company-departments').show();
		$('.my-company-navbar li').removeClass('navbar-active');
		$('#departments').addClass('navbar-active');
	});
	
// My Employees page JS
	$('.hidden-1').hide();
	$('.employees-search').show();
	$('#search_emp').addClass('navbar-active');
	$('#search_emp').bind('click',function(){
		$('.hidden-1').hide();
		$('.employees-search').show();
		$('.my-employees-navbar li').removeClass('navbar-active');
		$('#search_emp').addClass('navbar-active');
	});
	$('#add_emp').bind('click',function(){
		$('.hidden-1').hide();
		$('.employees-add').show();
		$('.sub-hidden').hide();
		$('.emp-personal-details').show();
		$('.my-employees-navbar li').removeClass('navbar-active');
		$('#add_emp').addClass('navbar-active');
	});
// My Employees sub-page start JS
		$('.sub-hidden').hide();
		$('.emp-personal-details').show();
		$('#nextbtn-1').bind('click',function(){
			$('.sub-hidden').hide();
			$('.emp-professional-details').show();
		});
		$('#nextbtn-2').bind('click',function(){
			$('.sub-hidden').hide();
			$('.emp-salary-details').show();
		});
		$('#nextbtn-3').bind('click',function(){
			$('.sub-hidden').hide();
			$('.emp-personal-details').show();
		});
		
// My Employees sub-page ends JS
	$('#remove_emp').bind('click',function(){
		$('.hidden-1').hide();
		$('.employees-remove').show();
		$('.my-employees-navbar li').removeClass('navbar-active');
		$('#remove_emp').addClass('navbar-active');
	});
	
// My Statutories page JS
		$('.hidden-2').hide();
		$('.statutories-pf-groups').show();	
		$('#pf_groups').addClass('navbar-active');
		$('#pf_groups').bind('click',function(){
			$('.hidden-2').hide();
			$('.statutories-pf-groups').show();
			$('.my-statutories-navbar li').removeClass('navbar-active');
			$('#pf_groups').addClass('navbar-active');
		});
		$('#pf_rates').bind('click',function(){
			$('.hidden-2').hide();
			$('.statutories-pf-rates').show();
			$('.my-statutories-navbar li').removeClass('navbar-active');
			$('#pf_rates').addClass('navbar-active');
		});
		$('#esi_groups').bind('click',function(){
			$('.hidden-2').hide();
			$('.statutories-esi-groups').show();
			$('.my-statutories-navbar li').removeClass('navbar-active');
			$('#esi_groups').addClass('navbar-active');
		});
		$('#esi_rates').bind('click',function(){
			$('.hidden-2').hide();
			$('.statutories-esi-rates').show();
			$('.my-statutories-navbar li').removeClass('navbar-active');
			$('#esi_rates').addClass('navbar-active');
		});
		$('#pt_groups').bind('click',function(){
			$('.hidden-2').hide();
			$('.statutories-pt-groups').show();
			$('.my-statutories-navbar li').removeClass('navbar-active');
			$('#pt_groups').addClass('navbar-active');
		});
		$('#pt_rates').bind('click',function(){
			$('.hidden-2').hide();
			$('.statutories-pt-rates').show();
			$('.my-statutories-navbar li').removeClass('navbar-active');
			$('#pt_rates').addClass('navbar-active');
		});
		
// My Payroll page JS
		$('.hidden-3').hide();
		$('.payroll-earning-heads').show();
		$('#earning_heads').addClass('navbar-active');
		$('#earning_heads').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-earning-heads').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#earning_heads').addClass('navbar-active');
		});
		$('#allowance_heads').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-allowance-heads').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#allowance_heads').addClass('navbar-active');
		});
		$('#perquisite_heads').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-perquisite-heads').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#perquisite_heads').addClass('navbar-active');
		});
		$('#deduction_heads').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-deduction-heads').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#deduction_heads').addClass('navbar-active');
		});
		$('#salary_structures').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-salary-structures').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#salary_structures').addClass('navbar-active');
		});
		$('#attendance_settings').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-attendance-settings').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#attendance_settings').addClass('navbar-active');
		});
		$('#leave_settings').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-leave-settings').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#leave_settings').addClass('navbar-active');
		});
		$('#lumpsum_settings').bind('click',function(){
			$('.hidden-3').hide();
			$('.payroll-lumpsum-settings').show();
			$('.my-payroll-navbar li').removeClass('navbar-active');
			$('#lumpsum_settings').addClass('navbar-active');
		});
});
/* My Company Module */
$(function company(){
	$('#my_company, #details').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'company_details'},success:function(result){
			console.log(result);
		}});
	});
	$('#branches').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'company_branches'},success:function(result){
			console.log(result);
		}});
	});
	$('#designations').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'company_designations'},success:function(result){
			console.log(result);
		}});
	});
	$('#departments').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'company_departments'},success:function(result){
			console.log(result);
		}});
	});
});

/* My Employee Module */

$(function employee(){
	$('#my_employee, #search_emp').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'search_emp'},success:function(result){
			console.log(result);
		}});
	});
	$('.search-btn').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'search_btn'},success:function(result){
			console.log(result);
		}});
	});
	$('#add_emp').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'add_emp'},success:function(result){
			console.log(result);
		}});
	});
	$('#nextbtn-1').bind('click',function(){
		var name,father_name,sex,m_status,dob,curr_address,per_address,mob_number,ph_number;
		name=$('#name').val();
		father_name=$('#father_name').val();
		if($('#male:checked').val())
		{
			sex='male';
		}
		else	
		{
			sex='female';
		}
		m_status=$('#m_status').val();
		dob=$('#dob').val();
		curr_address=$('#curr_address').val();
		per_address=$('#per_address').val();
		mob_number=$('#mob_number').val();
		ph_number=$('#ph_number').val();
		console.log(name);
		console.log(father_name);
		console.log(sex);
		console.log(m_status);
		console.log(dob);
		console.log(curr_address);
		console.log(per_address);
		console.log(mob_number);
		console.log(ph_number);
		$.ajax({url:"php/common.php",type:"POST",data:{action:'nextbtn_1',name:name,father_name:father_name,sex:sex,m_status:m_status,dob:dob,curr_address:curr_address,per_address:per_address,mob_number:mob_number,ph_number:ph_number},success:function(result){
		console.log(result);
		}});
	});
	$('#nextbtn-2').bind('click',function(){	
		var id,designation,department,doj,pan_number;
		id=$('#id').val();
		designation=$('#designation').val();
		department=$('#department').val();
		doj=$('#doj').val();
		pan_number=$('#pan_number').val();
		console.log(id);
		console.log(designation);
		console.log(department);
		console.log(doj);
		console.log(pan_number);
		$.ajax({url:"php/common.php",type:"POST",data:{action:'nextbtn_2'},success:function(result){
			console.log(result);
		}});
	});
	$('#nextbtn-3').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'nextbtn_3'},success:function(result){
			console.log(result);
		}});
	});
	$('#remove_emp').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'remove_emp'},success:function(result){
			console.log(result);
		}});
	});
});

/* My Statutories Module */

$(function statutories(){
	$('#my_statutories, #pf_groups').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'pf_groups'},success:function(result){
			console.log(result);
		}});
	});
	$('#pf_rates').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'pf_rates'},success:function(result){
			console.log(result);
		}});
	});
	$('#esi_groups').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'esi_groups'},success:function(result){
			console.log(result);
		}});
	});
	$('#esi_rates').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'esi_rates'},success:function(result){
			console.log(result);
		}});
	});
	$('#pt_groups').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'pt_groups'},success:function(result){
			console.log(result);
		}});
	});
	$('#pt_rates').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'pt_rates'},success:function(result){
			console.log(result);
		}});
	});
});

/* My Payroll Module */

$(function payroll(){
	$('#my_payroll, #earning_heads').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'earning_heads'},success:function(result){
			console.log(result);
		}});
	});
	$('#allowance_heads').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'allowance_heads'},success:function(result){
			console.log(result);
		}});
	});
	$('#perquisite_heads').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'perquisite_heads'},success:function(result){
			console.log(result);
		}});
	});
	$('#deduction_heads').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'deduction_heads'},success:function(result){
			console.log(result);
		}});
	});
	$('#salary_structures').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'salary_structures'},success:function(result){
			console.log(result);
		}});
	});
	$('#attendance_settings').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'attendance_settings'},success:function(result){
			console.log(result);
		}});
	});
	$('#leave_settings').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'leave_settings'},success:function(result){
			console.log(result);
		}});
	});
	$('#lumpsum_settings').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:'lumpsum_settings'},success:function(result){
			console.log(result);
		}});
	});
});

/* My Attendance Processor Module */

$(function attendanceprocessor(){
	$('#my_attendanceprocessor').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:''},success:function(result){
			console.log(result);
		}});
	});
});

/* My Salary Processor Module */

$(function salaryprocessor(){
	$('#my_salaryprocessor').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:''},success:function(result){
			console.log(result);
		}});
	});
});

/* My Tools Module */

$(function payroll(){
	$('#my_tools').bind('click',function(){	
		$.ajax({url:"php/common.php",type:"POST",data:{action:''},success:function(result){
			console.log(result);
		}});
	});
});
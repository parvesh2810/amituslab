$(document).ready(function(){
		$('.works-icon .down-icon').bind('click',function(){
			$('.works-submenu').fadeToggle();
		});
		$('.menu-open').bind('click',function(){
			$('.menu-open').toggleClass('menu-close');
			$('.list').slideToggle();
		});
		$('.read-more-icon').bind('click',function(){
			$('.read-more').toggle();
			$('.common-text').slideToggle();
		});
		
		$('#img_1 a').hover(function(){
			$('.img_1').attr('src',"img/img1-hover.png").show('slow');
		}
		,function(){
			$('.img_1').attr('src',"img/img1.png").show('slow');
		});
		
		$('#img_2 a').hover(function(){
			$('#img_2 img').attr("src","img/img2-hover.png").show('slow');
		}
		,function(){
			$('#img_2 img').attr("src","img/img2.png").show('slow');
		});
		
		$('#img_3 a').hover(function(){
			$('#img_3 img').attr("src","img/img3-hover.png").show('slow');
		}
		,function(){
			$('#img_3 img').attr("src","img/img3.png").show('slow');
		});
		$('#img_4 a').hover(function(){
			$('#img_4 img').attr("src","img/img1-hover.png").show('slow');
		}
		,function(){
			$('#img_4 img').attr("src","img/img1.png").show('slow');
		});
		$('#img_5 a').hover(function(){
			$('#img_5 img').attr("src","img/img2-hover.png");
		}
		,function(){
			$('#img_5 img').attr("src","img/img2.png");
		});
		$('#img_6 a').hover(function(){
			$('#img_6 img').attr("src","img/img3-hover.png");
		}
		,function(){
			$('#img_6 img').attr("src","img/img3.png");
		});
});
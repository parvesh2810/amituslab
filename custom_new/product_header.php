<?php
require_once('Init.php');

$user_state_type = "";

if(isset($_SESSION['user_state'])){
$user_state_type = $_SESSION['user_state'];
}

if($user_state_type == 'loggedin'){
echo '<header>  
<div id="header_main-container">
    <div id="header_container1" class="loggedin">
	    <div class="header_left">
		   <ul>
		      <li class="li_menu"><a href="home.php">Home</a></li>
			  <li class="li_menu" id="li_menu_about"><a href="javascript:void(0)">About Us</a></li>
			  <li class="li_menu" id="li_menu_more">
			  <a href="javascript:void(0)">Explore More</a>
			  <span id="li_menu_more_icon"></span>
			  </li>
		   </ul>
		</div>
		<div class="header_middle">
		   <img src="img/t-logo.png" />
		</div>
		<div class="header_right">
		   <ul>
		     <li class="li_sign" id="signup"><a href="javascript:void(0);" class="show_signup_modal">Sign Up</a><span class="stick">|</span></li>
		     <li class="li_sign" id="signin"><a href="javascript:void(0);" class="show_signin_modal">Sign In</a><span class="stick">|</span></li>
		     <li class="li_sign"><a href="contact-us.html">Support</a></li>
		   </ul>
		</div>   
		<div class="header_right_login">   
		   <ul>
			 <li class="li_sign"><a href="contact-us.html">SUPPORT</a><span class="stick">|</span></li>
		     <li class="" id="user_loggedin_icon"><a href="javascript:void(0);" class="">Hi, '.$_SESSION['current_user_name'].'</a></li>
		   </ul>
		</div>   
	</div>
	 
	

</div>
</header>';
}

else{
echo '<header>  
<div id="header_main-container">
    <div id="header_container1">
	    <div class="header_left">
		   <ul>
		      <li class="li_menu"><a href="home.php">Home</a></li>
			  <li class="li_menu" id="li_menu_about"><a href="javascript:void(0)">About Us</a></li>
			  <li class="li_menu" id="li_menu_more">
			  <a href="javascript:void(0)">Explore More</a>
			  <span id="li_menu_more_icon"></span>
			  </li>
		   </ul>
		</div>
		<div class="header_middle">
		   <img src="img/t-logo.png" />
		</div>
		<div class="header_right">
		   <ul>
		     <li class="li_sign" id="signup"><a href="javascript:void(0);" class="show_signup_modal">Sign Up</a><span class="stick">|</span></li>
		     <li class="li_sign" id="signin"><a href="javascript:void(0);" class="show_signin_modal">Sign In</a><span class="stick">|</span></li>
		     <li class="li_sign"><a href="contact-us.html">Support</a></li>
		   </ul>
		</div>   
		<div class="header_right_login">   
		   <ul>
			 <li class="li_sign"><a href="contact-us.html">SUPPORT</a><span class="stick">|</span></li>
		     <li class="" id="user_loggedin_icon"><a href="javascript:void(0);" class=""></a></li>
		   </ul>
		</div>   
	</div>
	 
	

</div>
</header>';
}


?>
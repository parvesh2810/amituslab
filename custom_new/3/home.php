<?php
require_once('../Init.php');

$shops_arr = $shop->get_featured_shops();
$_SESSION['current_page'] = $_SERVER['PHP_SELF'];
$user_state_type = $_SESSION['user_state'];

$username = "";
$password = "";
$login_res = "";

if(isset($_POST['login_username']) && !empty($_POST['login_username'])){

$username = $utils->protect($_POST['login_username']);
$password = $utils->protect($_POST['login_password']);

$login_res = $user->user_login($username,$password);
if($login_res['result'] == "success"){
$_SESSION['user_state'] = 'loggedin';
header('Location: '.$_SESSION['current_page']);
}else if($login_res['result'] == "nosuccess"){
echo 'Username or password is incorrect.';
}

}



$first_name = "";
$last_name = "";
$email = "";
$password = "";
$phone = "";
$zip_code = "";
$user_arr = array();

if(isset($_POST['firstname']) && !empty($_POST['firstname']) && isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])){

$first_name = $utils->protect($_POST['firstname']);
$last_name = $utils->protect($_POST['lastname']);
$email = $utils->protect($_POST['email']);
$password = $utils->protect($_POST['password']);
$confirm_password = $utils->protect($_POST['confirmation']);
$phone = $utils->protect($_POST['phone']);
$zip_code = $utils->protect($_POST['zip_code']);
if($confirm_password == $password){

$user_arr = $user->add_user($first_name,$last_name,$email,$password,$phone,$zip_code);
echo $user_arr['result'];
if($user_arr['result'] == 'user added'){
$_SESSION['user_state'] = 'loggedin';
header('Location '.$_SESSION['current_page']);
}else if($user_arr['result'] == 'Email already present'){
echo 'This email is already present. Please use some other email address.';
}

}else{
echo 'Passwords do not match.';
}

}






?>


<!doctype html>
<html>
<head>
<title>Home</title>
<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
<link rel="stylesheet" type="text/css" href="css/header.css" />
<link rel="stylesheet" type="text/css" href="css/home.css" />
<link rel="stylesheet" type="text/css" href="css/footer.css" />


<style>
@font-face{
src:url("fonts/Raleway-Bold.otf");
font-family:'Raleway-bold'
}

@font-face{
src:url("fonts/Raleway-Regular.otf");
font-family:'Raleway-regular'
}

@font-face{
src:url("fonts/Roboto-Bold.ttf");
font-family:'Roboto-bold'
}

@font-face{
src:url("fonts/Roboto-Regular.ttf");
font-family:'Roboto-regular'
}

@font-face{
src:url("fonts/Roboto-Light.ttf");
font-family:'Roboto-light'
}

@font-face{
src:url("fontello/fontello/fontello.ttf");
font-family:'icon1'
}

@font-face{
src:url("fontello/fontello1/fontello.ttf");
font-family:'icon2'
}

@font-face{
src:url("fontello/fontello2/fontello.ttf");
font-family:'icon3'
}

</style>

<script src="js/jquery-2.1.1.js"></script>
<script src="js/common-js.js"></script>
<script>
/*
$(function(){
$(window).scroll(function(){
$("#indent_custom").css({
'top':-($(this).scrollTop()/3)+'px'
});
});

});
*/
function password_validation(value1)
{
	var p1=$('#signup_password').val();
	var p2=$('#confirm_password').val();
	if(p1==value1)
	{
		
	if(p2=='')
	{
		
	}
	else {
	
	if(p1==p2) 
	{
		alert("Match");
	}
	else 
	{
		alert("Mis-match");
	}
	}
}
	
}
$(function(){

/*
$("#li_menu_about").click(function(e){
$("#container_aboutus").toggle('slow', 'swing');
e.stopPropagation();
})

$(document).click(function(evt){
$("#container_aboutus:visible").hide();
});
*/

$('.explore_watch').bind('click',function(){
$('.home_lightbox').addClass('visible');
	$('.youtube-video-container').addClass('opacity-1');
});

$('.show_signup_modal').bind('click',function(){
$('.home_lightbox').addClass('visible');
$('.signup_modal').addClass('visible');
});


$('.show_signin_modal').bind('click',function(){
$('.home_lightbox').addClass('visible');
$('.signin_modal').addClass('visible');
});

$('.ppp5 a').bind('click',function(){
	$('.signup_modal').removeClass('visible');
	$('.signin_modal').addClass('visible');
});
$('.pp4 a').bind('click',function(){
	$('.signin_modal').removeClass('visible');
	$('.signup_modal').addClass('visible');
});


$('.pp2').bind('click',function(){
	$('.signin_modal').removeClass('visible');
	$('.signup_modal').removeClass('visible');
	$('#outer_FP').addClass('visible');
});

$('.home_lightbox').bind('click',function(){
$('.home_lightbox').removeClass('visible');
$('.signup_modal').removeClass('visible');
$('.signin_modal').removeClass('visible');
$('.youtube-video-container').removeClass('opacity-1');
$('#outer_FP').removeClass('visible');
});


$("#li_menu_about").click(function(e){
$("#container_explore").removeClass('show_aboutus');
if($("#container_aboutus").hasClass('show_aboutus')){
$("#container_aboutus").removeClass('show_aboutus');
}else{
$("#container_aboutus").addClass('show_aboutus');
}
e.stopPropagation();
})
$(document).click(function(evt){
$("#container_aboutus").removeClass('show_aboutus');
});


$("#li_menu_more").click(function(e){
console.log('done');
$("#container_aboutus").removeClass('show_aboutus');
if($("#container_explore").hasClass('show_aboutus')){
$("#container_explore").removeClass('show_aboutus');
}else{
$("#container_explore").addClass('show_aboutus');
}
e.stopPropagation();
})
$(document).click(function(evt){

});
});

</script>
<!--
<script>
$(function(){

$("#signup").click(function(){
$("#outer_signup").show();


});
});

</script>-->
</head>
<body>
	<div class="youtube-video-container">
		<iframe width="560" height="315" src="//www.youtube.com/embed/jSPTF-WBkqw" frameborder="0" allowfullscreen></iframe>
	</div>
    <div id="container_aboutus" class="container_aboutus">  <!--submenu for aboutus-->
	    <p class="p_aboutus">In 1981, seven engineers started Infosys Limited with just US$250. From the beginning, the company was founded on the principle of building and implementing great ideas that drive progress for clients and enhance lives through enterprise solutions. For over three decades, we have been a company focused on bringing to life great ideas and enterprise solutions that drive progress for our clients.</p>
	</div>
	
	
	 <div id="container_explore" class="container_explore"> <!--submenu for explore more-->
     <div id="container_explore1">
	       <div class="explore_item">
		     <div class="explore_heading">
			    <p class="p20">Most Popular</p>
			 </div>
            <div class="explore_content">			 
			  <ul>
			     <li class="explore_li"><a href="#">Pizza Hut</a></li>
			     <li class="explore_li"><a href="#">Subway</a></li>
			     <li class="explore_li"><a href="#">Mast kalander</a></li>
			     <li class="explore_li"><a href="#">Baskin Robbins</a></li>
			     <li class="explore_li"><a href="#">Yo China</a></li>
			     <li class="explore_li"><a href="#">Pind Bolluchi</a></li>
			  </ul>
			 </div> 
		   </div>
		   
		   <div class="explore_item">
		      <div class="explore_heading">
			    <p class="p20">How it works</p>
			 </div>
             <div class="explore_content">
			     <div>
					<iframe class="explore_video" width="100" height="100" src="//www.youtube.com/embed/jSPTF-WBkqw" frameborder="0" allowfullscreen></iframe>
				 </div>
				 <div><a class="explore_watch" href="#">Watch >></a></div>
			 </div>			 
		   </div>
		   
		   <div class="explore_item">
		      <div class="explore_heading">
			    <p class="p20">Let's Be Friends</p>
			 </div>
			 <div class="explore_content">
			 <div class="explore_social">
			    <ul>
				    <li class="e_social_icon"><a href="#"><img src="img/facebook.png"/></a></li>
				    <li class="e_social_icon"><a href="#"><img src="img/twitter.png"/></a></li>
				    <li class="e_social_icon"><a href="#"><img src="img/instagram.png"/></a></li>
				</ul>
			 </div>
			 <div class="explore_social_name">
			      <li class="e_social_name"><a target="_ " href="https://www.facebook.com/pages/Foodylite/1457529654520045" >Facebook</a></li>
			      <li class="e_social_name"><a href="#">Twitter</a></li>
			      <li class="e_social_name"><a href="#">Instagram</a></li>
			 </div>
			 </div>
		   </div>

		   <div class="explore_item">
		      <div class="explore_heading">
			    <p class="p20">Let's Be Friends</p>
			  </div>
			  <div class="explore_content">
			     <p class="p21">Email: chirag@foodylite.com</p>
			     <p class="p21">Phone:9999999999</p>
			  </div>
		  </div> 
		   
	   </div>
	</div> <!--end of submenu-->
	
	<!--
<header>  
<div id="header_main-container">
    <div id="header_container1">
	    <div class="header_left">
		   <ul>
		      <li class="li_menu"><a href="home.php">Home</a></li>
			  <li class="li_menu" id="li_menu_about"><a href="javascript:void(0)">About Us</a></li>
			  <li class="li_menu" id="li_menu_more">
			  <a href="javascript:void(0)">Explore More</a>
			  </li>
		   </ul>
		</div>
		<div class="header_middle">
		   <img src="img/t-logo.png" />
		</div>
		<div class="header_right">
		   <ul>
		     <li class="li_sign" id="signup"><a href="javascript:void(0);" class="show_signup_modal">Sign Up</a><span class="stick">|</span></li>
		     <li class="li_sign" id="signin"><a href="javascript:void(0);" class="show_signin_modal">Sign In</a><span class="stick">|</span></li>
		     <li class="li_sign"><a href="contact-us.html">Support</a></li>
		   </ul>
		</div>   
		<div class="header_right_login">   
		   <ul>
			 <li class="li_sign"><a href="contact-us.html">SUPPORT</a><span class="stick">|</span></li>
		     <li class="" id="user_loggedin_icon"><a href="javascript:void(0);" class="">Hi, <?php echo $_SESSION['current_user_name'];  ?></a></li>
		   </ul>
		</div>   
	</div>
	 
	

</div>
</header>
-->

<?php
require_once('header.php');

?>

<div id="main-container">
    <div id="container1">
	   <div id="banner">  <!--banner -->
	      <div id="banner_container">
			<div class="banner_container1"> 
			 <img src="img/img1.png"/>
			</div> 
			<div class="banner_container1"> 
			 <a id="btn1_btn1" href="shop_list.php">ORDER NOW</a>
			</div> 
		  </div>
	   </div>  <!--End banner-->
	   
	   <div id ="indent_custom"> <!--custom images-->
	      <div class="indent_custom_custom" id="custom1">
		     <div class="custom_left">
			    <div class="custom_para">
				   <p class="p1">Step 1</p>
                </div>
                <div class="custom_img">
				  <img src="img/img2.png"/>
                </div>
                <div class="custom_content">
				  <p class="p2">Tell us Where you are</p>
                </div>
                <div class="custom_desc">
				   <p class="p3">And what you are looking for</p>
                </div>				
			 </div>
		  </div>
		  
		  <div class="indent_custom_custom" id="custom2">
		     <div class="custom_left">
			    <div class="custom_para para1">
				   <p class="p1">Step 2</p>
                </div>
                <div class="custom_img">
				  <img src="img/img3.png"/>
                </div>
                <div class="custom_content">
				  <p class="p2">Choose a confectionery</p>
                </div>
                <div class="custom_desc">
				   <p class="p3">We have multiple confectionery stores selling online</p>
                </div>				
			 </div>
		  </div>
		  
		  <div class="indent_custom_custom">
		     <div class="custom_left">
			    <div class="custom_para">
				   <p class="p1">Step 3</p>
                </div>
                <div class="custom_img">
				  <img src="img/img4.png"/>
                </div>
                <div class="custom_content">
				  <p class="p2">pay by cash or card</p>
                </div>
                <div class="custom_desc">
				   <p class="p3">It's quick, easy and totally secure</p>
                </div>				
			 </div>
		  </div>
		</div>   <!--End of custom images-->
		
		<div id="container2">  <!--trending image-->
		   <div id="container2_img">
		     <img src="img/img5.png"/>
		   </div>
		</div>  <!--End of trending image-->
		
		<div id="container3">
		   <div id="container3_image">
		   
		   <?php
		   foreach($shops_arr as $sa){
		     echo '<div class="container3_img_desc">
			    <div class="container3_img1" id="desc2">
				   <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1" href="http://amituslab.com/magento4/custom/3/shop_list.php">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">'.$sa['name'].'</a></p>
				   <p class="p5">'.$sa['street_address'].'!!</p>
				</div>
				
			 </div>';
			 }
			 
			 ?>
                		 
			 <!--
			 <div class="container3_img_desc">
			    <div class="container3_img1">
				   <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div> 
			 
			 <div class="container3_img_desc">
			    <div class="container3_img1">
				  <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div>

			 <div class="container3_img_desc">
			    <div class="container3_img1">
				 <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div> 
			 <div class="container3_img_desc">
			    <div class="container3_img1">
				  <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div> 
			 
			 <div class="container3_img_desc">
			    <div class="container3_img1">
				  <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div> 
			 
			 <div class="container3_img_desc">
			    <div class="container3_img1">
				   <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div> 
			 
			 <div class="container3_img_desc">
			    <div class="container3_img1">
				  <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div> 
			 
			 <div class="container3_img_desc">
			    <div class="container3_img1">
				  <div class="img-27"><img src="img/gimg/img27.png"/>
							<a  class="desc1">
							<p class="p50">VIEW</p>
					</a>
				   </div>
				</div>
				<div class="img1_desc">
				   <p class="p4"><a href="#">Lorem Ipsum</a></p>
				   <p class="p5">Nice Shop, Coming Soon!!</p>
				</div>
			 </div>
			 -->
		   </div>
		   
		</div>  <!--End of container3-->
		
	    <div id="container4">
		    <div id="container4_f">
               <p class="p6">Reviews</p>
			</div>
			<div id="container4_s">
			    <div class="container4_second">
				  <div class="container4_second_image">
				    <a href="#"><img src="img/chirag.jpg"/></a>
				  </div>
				  <div class="container4_second_content">
				     <p class="p7"><a href="#">Chirag Gambhir</a><br/>
					 <span class="p8">Sept 9, 2014</span></p><br/>
					 <p class="p9"><q>I like how this looks!</q></p>
				  </div>
				</div> 
				
				<div class="container4_second scont">
				  <div class="container4_second_image">
				    <a href="#"><img src="img/akshay.jpg"/></a>
				  </div>
				  <div class="container4_second_content">
				     <p class="p7"><a href="#">Akshay Prabhu</a><br/>
					 <span class="p8">Sept 9, 2014</span></p><br/>
					 <p class="p9"><q>Makes my life easier!</q></p>
				  </div>
				</div>
			</div>
		</div>   <!--End of container4-->

        <div id="container5">
		   <div id="container5_1">
		      <div class="container5_img">
		        <img src="img/img6.png"/>
			  </div>
			  <div class="container5_content">
			    <p class="p10">Connect with us on facebook and we will send you exciling<br/> personalized offers for your happy moments </p>
			  </div>
			  <div class="container5_button">
			   <a id="btn1_btn2" target="_ " href="https://www.facebook.com/pages/Foodylite/1457529654520045" >Connect with Facebook</a
			  </div>
			  <div class="container5_content">
			     <p class="p10">Don't miss out on our great offers<br/> Receive deals from all our top restaurants via e-mail </p>
			  </div>
		   </div>
		</div>  <!--End of container5-->
 
       <footer>  <!--Footer-->
	    <div id="footer_footer">
	      <div class="footer_container">
		    <div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Popular Restaurants</p>
			  </div>	
			   <ul>
			      <li class="footer_li"><a href="#">Pizza Hut</a></li>
			      <li class="footer_li"><a href="#">Subway</a></li>
			      <li class="footer_li"><a href="#">Mast Kalander</a></li>
			      <li class="footer_li"><a href="#">Bascin Robbins</a></li>
			      <li class="footer_li"><a href="#">Yo China</a></li>
			      <li class="footer_li"><a href="#">Pind Bolluchi</a></li>
			      <li class="footer_li"><a href="#">Dana Choga</a></li>
			      <li class="footer_li"><a href="#">Mamagoto</a></li>
			      <li class="footer_li"><a href="#">Chin Chin</a></li>
			      <li class="footer_li"><a href="#">Arabesque</a></li>
				  
			   </ul>
			</div>
			
			<div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Popular Cuisines</p>
			  </div>	
			   <ul>
			      <li class="footer_li"><a href="#">Bengali</a></li>
			      <li class="footer_li"><a href="#">Biryani</a></li>
			      <li class="footer_li"><a href="#">Cakes-Bakery</a></li>
			      <li class="footer_li"><a href="#">Chinese</a></li>
			      <li class="footer_li"><a href="#">Continental</a></li>
			      <li class="footer_li"><a href="#">Fast Food</a></li>
			      <li class="footer_li"><a href="#">Hyderabadi</a></li>
			      <li class="footer_li"><a href="#">Ice creams</a></li>
			      <li class="footer_li"><a href="#">Italian</a></li>
			      <li class="footer_li"><a href="#">Japanese</a></li>
				  
			   </ul>
			</div>
			
			<div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Foodylite</p>
			  </div>	
			   <ul>
			      <li class="footer_li"><a href="#">About us</a></li>
			      <li class="footer_li"><a href="#">foodylite.in press</a></li>
			      <li class="footer_li"><a href="#">Contact</a></li>
			      <li class="footer_li"><a href="#">Terms and Conditions</a></li>
			      <li class="footer_li"><a href="#">Privacy Policy</a></li>
				  
			   </ul>
			</div>
			
			<div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Be a contributor</p>
			  </div>	
			  <p class="p12">If you wish to share your recipes through our blog, please leave your email:</p>
			  <div class="input1">
			    <input class="input2" type="text" placeholder="Enter your Email id"/>
			  </div>
			  <div class="submit">
			    <button id="btn1_btn3"><a href="#">SUBMIT</a></button>
			  </div>
			</div>
		 </div>
		 <div id="footer_last">
		    <div class="last_item">
			   <p class="p13">&copy foodylite 2014</p>
			</div>
			
			<div class="last_item">
			   <p class="p14">We currently deliver in Delhi/NCR</p>
			</div>
			
			<div class="last_item" id="social">
			   <ul>
			      <li class="social"><a href="#"><img src="img/facebook.png"/></a></li>
				  <li class="social"><a href="#"><img src="img/twitter.png"/></a></li>
				  <li class="social"><a href="#"><img src="img/instagram.png"/></a></li>
			   </ul>
			</div>
			
		 </div>
	  </div>
	</footer>  <!--End of footer-->
</div>
</div>

<!--SIGN In-->

   <div id="container_SI" class="lightbox_options_modal signin_modal">
        <div id="login_img">
		   <img src="img/login.png"/>
		</div>
		
		<div id="login_form">
		   <div class="login_form_left">
		      <div id="login_buttons">
			     <div class="button_google">
				    <button id="btn2_1"><a href="#">Sign in with Google</a></button>
				 </div>
				 <div class="border_SI_3"></div>
				 
				 <div class="button_google">
				    <button id="btn2_2"><a href="#">Login with Facebook</a></button>
				 </div>
				 <div class="border_SI_4"></div>
			  </div>
		   </div>
		   <div class="login_form_middle">
		      <p class="pp1">OR</p>
		   </div>
		   
		   <div class="border_SI_1">
		   </div>
		   
		   <div class="border_SI_2">
		   
		   </div>
		   
		   <div class="login_form_left1">
		       <div id="form_input">
		       <form id="signin_form" action="home.php" method="post">
			      <div class="form_input1">
				     <input type="email" required name="login_username" class="text1" placeholder="Your Email"/>
				  </div>
				  <div class="form_input1">
				     <input type="password" required name="login_password" class="text1" placeholder="Password" pattern="[A-Za-z0-9!@#$%/^&*]{8,20}"/>
				  </div>
				  
			   </div>
		   </div>
		</div>
		
		<div id="button_log">
		    <div class="log_forgot">
			   <input type="submit" id="btn2_3" value="LOG IN" />
			</div>
			</form>
			<div class="log_forgot1">
			   <p class="pp2"><a href="#">Forgot your Password?</a></p>
			</div>
		</div>
	     
        <div id="login_account">
		   <p class="pp3">Don't have an Account ?</p>
		   <p class="pp4"><a href="#">Sign Up</a></p>
		</div>		 
	 </div> 
	 <!--End of SignIn--> 
	 
	  <!--FORGOT YOUR PASSWORD-->
	<div id ="outer_FP">
		   <div id="inner_FP">
		      <div class="FP_container1">
			     <img src="img/find-pass.png"/>
			  </div>
              <div class="FP_container2">
			     <input class="FP_input" placeholder="Your Email"/>
			  </div>
              <div class="FP_container3">
			     <button class="FP_submit"><a href="javascript:void(0)">Submit</a></button>
			  </div>
              <p class="FP_p1">To retrieve your password, enter the email address</p>			  
		   </div>
        </div>
		
		<!--FORGOT YOUR PASSWORD-->

   <!--Sign Up-->
 <div id="outer_signup" class="lightbox_options_modal signup_modal">
   <div id="main-container_SU">
       <div id="container_SU">
	        <div id="container1_SU">
			  <div class="container1_button">
			     <button id="btn3_1"><a href="#">Signup with Facebook</a></button>
			  </div>
			  <div class="container1_p">
			     <p class="ppp1">We will never post anything on your wall. Promise! </p>
			  </div>
			</div>
			
			<div id="container2_SU">
			   <p class="ppp2">OR</p>
			</div>
			
			<div class="border_border">
			</div>
			<div class="border_border1">
			</div>
			
			<div class="border_border2">
			</div>
			
			<div id="container3_SU">
			   
			   <div class="container3_input">
			   <form id="signup_form" action="home.html" method="post">
			       <div class="input_name1">
				      <input type="text" required class="name" name="firstname" placeholder="First Name" pattern="[A-Za-z]{1,30}" title="Numbers are not allowed" />
				   </div>
				   
				   <div class="input_name2">
				      <input type="text" required class="name" name="lastname" placeholder="Last Name" pattern="[A-Za-z]{1,30}" title="Numbers are not allowed"/>
				   </div>
			   </div>
			   
			   <div class="container3_input">
			     <div class="input_name">
				      <input type="email" required class="input_email" name="email" placeholder="Email"/>
				   </div>
			   </div>
			   
			   <div class="container3_input">
			       <div class="input_name">
				      <input id="signup_password" type="password" required class="input_email input_passwd" name="password" pattern="[A-Za-z0-9!@#$%/^&*]{8,20}"  placeholder="Password( Minimum 8 Characters)" onblur="password_validation(this.value)"/>
				   </div>
			   </div>
			   <div class="container3_input">
			     <div class="input_name">
				      <input id="confirm_password" type="password" required class="input_email" name="confirmation" pattern="[A-Za-z0-9!@#$%/^&*]{8,20}" placeholder="Confirm Password" onblur="password_validation(this.value)"/>
				   </div>
			   </div>
			   
			   <div class="container3_input">
			     <div class="input_name3">
				      <input type="text" class="name3" name="phone" placeholder="" pattern="[0-9]{10,10}" title="Please enter 10 digit mobile number"/>
					  <div class="fixed_contact">
					     +91
					  </div>
				   </div>
				   
				   <div class="input_name4">
				      <input type="text" class="name" name="zip_code" placeholder="Zip Code" pattern="[0-9]{1,6}"/>
				   </div>
			   </div>
			</div>
			
			<div id="container4_SU">
			   <div class="container4_all">
			     <input type="submit" id="btn3_btn2" value="SIGN UP" />
			   </div>
			   </form>
			   
			   <div class="container4_all all1">
			     <p class="ppp4">Already have an Account ?<span class="ppp5"><a href="#">Log In</a></span></p>
			   </div>
			   
			   <div class="container4_all">
			     <p class="ppp6">By signing up, you agree to the privacy policy and terms and conditions</p>
			   </div>
			   
			</div>
	   </div>
   </div>  
   </div> 
   
   <div class="home_lightbox">
   
   </div>
   
   
</body>
</html>
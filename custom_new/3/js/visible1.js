$(function(){

 var state=1;
 var s1,s2,s3,s4;
 current_state='step1';
 
 
 
$('.choice_2, .c4_date, .q8, .q9, .q10').hide();
 $('.confirm_order_hidden').hide();
 $('.sub-form-hidden').css('opacity','0');
 $('.sub-form-hidden').css('height','0');
 if($('.sub-form-hidden').hasClass('confirm_order_logged')){
 $('.sub-form-hidden').css('opacity','1');
 }
 $("#container3_product2").show();
 $('.c4_comp_name').hide();

 
 // object for next button
 var objnext={
	step1:"#container3_product2",
	step2:"#container10",
	step3:"#container4",
	step4:"#container5"
}
// object for back button
var objback={
	step1:"#container3_product2",
	step2:"#container10",
	step3:"#container4",
	step4:"#container5"
}
					
//Functionality of next button srt 

	$(".cm-btn-next").click(function(){
		var keynext;
		keynext=$(this).attr('data-step');
		
		//Form validation Start Part 
			
		
		//Form validation end Part 
			$(".confirm_order_hidden").hide();
			$(objnext[keynext]).show();
			if(objnext[keynext]=="#container10")
			{
				$("#btn1_btn1").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
				$("#btn1_btn2").css({'background':'brown', 'border':'1px solid brown'});
					$('#btn1_btn1').click(function(){
						$(".confirm_order_hidden").hide();
						$("#container3_product2").show();
						$("#btn1_btn1").css({'background':'brown', 'border':'1px solid brown'});
						$("#btn1_btn2, #btn1_button3, #btn1_btn4").css({'background':'rgba(0,0,0,0.09)', 'border':'1px solid rgba(0,0,0,0.09)'});
					});
			}
			if(objnext[keynext]=="#container4")
			{
				$("#btn1_btn1, #btn1_btn2").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
				$("#btn1_button3").css({'background':'brown', 'border':'1px solid brown'});
				state=3;
			}
			if(objnext[keynext]=="#container5")
			{
				$("#btn1_btn1, #btn1_btn2, #btn1_button3 ").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
				$("#btn1_btn4").css({'background':'brown', 'border':'1px solid brown'});
				state=4;
			}
		//}
	});
	
	//Functionality of next button end 
	
	//Functionality of back button srt 
	
	$(".cm-btn-back").click(function(){
		var keyback;
		$(".confirm_order_hidden").hide();
		keyback=$(this).attr('data-step');
		$(objback[keyback]).show();
		if(objback[keyback]=="#container3_product2")
		{
			$("#btn1_btn1").css({'background':'brown', 'border':'1px solid brown'});
			$("#btn1_btn2").css({'background':'rgba(0,0,0,0.09)', 'border':'1px solid rgba(0,0,0,0.09)'});
			state=1;
			
		}
		if(objback[keyback]=="#container10")
		{
			$("#btn1_btn1").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
			$("#btn1_button3").css({'background':'rgba(0,0,0,0.09)', 'border':'1px solid rgba(0,0,0,0.09)'});
			$("#btn1_btn2").css({'background':'brown', 'border':'1px solid brown'});
			state=2;
		}
	});
	
	//Functionality of back button end 
	
	$("#create_my_account_check").click(function(){
		$('.sub-form-hidden').css('opacity','1');
		$('.sub-form-hidden input').attr('required','true');
		$('#c10_input_email').attr('required','true');	
		$('.sub-form-hidden').css('height','auto');
		
	});
	$("#continue_as_guest_check").click(function(){
		$('.sub-form-hidden').css('opacity','0');
		$('.sub-form-hidden input').removeAttr('required');
		$('#c10_input_email').attr('required','true');
		$('.sub-form-hidden').css('height','0');	
	});
	$("#office_delivery_check").click(function(){
		$('.c4_comp_name').toggle();
	});
	$(".s1_check1").click(function(){
		$('.choice_2, .c4_date, .q8, .q9, .q10').hide();
	});
	$(".s1_check2").click(function(){
		$('.choice_2, .c4_date, .q8, .q9, .q10').show();
	});
				$('#c10_button1').click(function(){
				var email_value=$('#c10_input_email').val();
				var patt = new RegExp("[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+[-0-9a-zA-Z.+_]\.[a-zA-Z0-9]{2,20}");
				var test=patt.test(email_value);
				if(!email_value){
					sweetAlert("Oops...", "Enter Email Address First!", "warning");
					return false;
				}
				else if(document.getElementById("continue_as_guest_check").checked && test)
				{
						$(".confirm_order_hidden").hide();
						$('#container4').show();
						$("#btn1_btn1, #btn1_btn2").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
						$("#btn1_button3").css({'background':'brown', 'border':'1px solid brown'});
						
						$('#btn1_btn2').click(function(e){
			
							$(".confirm_order_hidden").hide();
							$("#container10").show();
							$("#btn1_btn1").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
							$("#btn1_btn2").css({'background':'brown', 'border':'1px solid brown'});
							$("#btn1_button3, #btn1_btn4").css({'background':'rgba(0,0,0,0.09)', 'border':'1px solid rgba(0,0,0,0.09)'});
					});
					return false;
				}
				else if(document.getElementById("create_my_account_check").checked && test)
				{
						$('#form_10').submit(function(){
						$(".confirm_order_hidden").hide();
						$('#container4').show();
						$("#btn1_btn1, #btn1_btn2").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
						$("#btn1_button3").css({'background':'brown', 'border':'1px solid brown'});
						
						$('#btn1_btn2').click(function(e){
			
							$(".confirm_order_hidden").hide();
							$("#container10").show();
							$("#btn1_btn1").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
							$("#btn1_btn2").css({'background':'brown', 'border':'1px solid brown'});
							$("#btn1_button3, #btn1_btn4").css({'background':'rgba(0,0,0,0.09)', 'border':'1px solid rgba(0,0,0,0.09)'});
					});
						return false;
					});
				}
				else
				{
					sweetAlert("Oops...", "Please Enter a Valid Email Address", "warning");
					return false;
				}
						
				});	
				
				
				$('#form_4').submit(function(){
						$(".confirm_order_hidden").hide();
						$('#container5').show();
						$("#btn1_btn1, #btn1_btn2, #btn1_button3 ").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
						$("#btn1_btn4").css({'background':'brown', 'border':'1px solid brown'});
						
						$('#btn1_button3').click(function(e){
			
							$(".confirm_order_hidden").hide();
							$("#container4").show();
							$("#btn1_btn1, #btn1_btn2").css({'background':'rgba(246,201,14,1)', 'border':'1px solid rgba(246,201,14,1)'});
							$("#btn1_button3").css({'background':'brown', 'border':'1px solid brown'});
							$("#btn1_btn4").css({'background':'rgba(0,0,0,0.09)', 'border':'1px solid rgba(0,0,0,0.09)'});
					});
						return false;
				});	

});

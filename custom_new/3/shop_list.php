<?php
require_once('../Init.php');

$shops = $shop->get_shops();

$_SESSION['current_page'] = $_SERVER['PHP_SELF'];

$username = "";
$password = "";
$login_res = "";

if(isset($_POST['login_username']) && !empty($_POST['login_username'])){

$username = $utils->protect($_POST['login_username']);
$password = $utils->protect($_POST['login_password']);

$login_res = $user->user_login($username,$password);
if($login_res['result'] == "success"){
header('Location: '.$_SESSION['current_page']);
}else if($login_res['result'] == "nosuccess"){
echo 'Username or password is incorrect.';
}

}



$first_name = "";
$last_name = "";
$email = "";
$password = "";
$phone = "";
$zip_code = "";
$user_arr = array();

if(isset($_POST['firstname']) && !empty($_POST['firstname']) && isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])){

$first_name = $utils->protect($_POST['firstname']);
$last_name = $utils->protect($_POST['lastname']);
$email = $utils->protect($_POST['email']);
$password = $utils->protect($_POST['password']);
$confirm_password = $utils->protect($_POST['confirmation']);
$phone = $utils->protect($_POST['phone']);
$zip_code = $utils->protect($_POST['zip_code']);
if($confirm_password == $password){

$user_arr = $user->add_user($first_name,$last_name,$email,$password,$phone,$zip_code);
echo $user_arr['result'];
if($user_arr['result'] == 'user added'){
header('Location '.$_SESSION['current_page']);
}else if($user_arr['result'] == 'Email already present'){
echo 'This email is already present. Please use some other email address.';
}

}else{
echo 'Passwords do not match.';
}

}


?>




<!doctype html>
<html>
<head>
<title>Shoplist</title>
<link rel="stylesheet" type="text/css" href="css/common-skin.css"/>
<link rel="stylesheet" type="text/css" href="css/header.css" />
<link rel="stylesheet" type="text/css" href="css/Shoplist.css">
<link rel="stylesheet" type="text/css" href="css/footer.css" />


<style>
@font-face{
src:url("fonts/Roboto-Bold.ttf");
font-family:'Roboto-bold'
}

@font-face{
src:url("fonts/Roboto-Regular.ttf");
font-family:'Roboto-regular'
}

@font-face{
src:url("fontello/fontello3/fontello.ttf");
font-family:'icon4'
}

@font-face{
src:url("fontello/fontello/fontello.ttf");
font-family:'icon1'
}
@font-face{
src:url("fontello/fontello1/fontello.ttf");
font-family:'icon2'
}

@font-face{
src:url("fontello/fontello2/fontello.ttf");
font-family:'icon3'
}

</style>
<script src="js/jquery-2.1.1.js"></script>
<script>
/*
$(function(){
$(window).scroll(function(){
$("#indent_custom").css({
'top':-($(this).scrollTop()/3)+'px'
});
});

});
*/

$(function(){

/*
$("#li_menu_about").click(function(e){
$("#container_aboutus").toggle('slow', 'swing');
e.stopPropagation();
})

$(document).click(function(evt){
$("#container_aboutus:visible").hide();
});
*/

$('.explore_watch').bind('click',function(){
$('.home_lightbox').addClass('visible');
	$('.youtube-video-container').addClass('opacity-1');
});

$('.show_signup_modal').bind('click',function(){
$('.home_lightbox').addClass('visible');
$('.signup_modal').addClass('visible');
});


$('.show_signin_modal').bind('click',function(){
$('.home_lightbox').addClass('visible');
$('.signin_modal').addClass('visible');
});

$('.ppp5 a').bind('click',function(){
	$('.signup_modal').removeClass('visible');
	$('.signin_modal').addClass('visible');
});
$('.pp4 a').bind('click',function(){
	$('.signin_modal').removeClass('visible');
	$('.signup_modal').addClass('visible');
});


$('.pp2').bind('click',function(){
	$('.signin_modal').removeClass('visible');
	$('.signup_modal').removeClass('visible');
	$('#outer_FP').addClass('visible');
});

$('.home_lightbox').bind('click',function(){
$('.home_lightbox').removeClass('visible');
$('.signup_modal').removeClass('visible');
$('.signin_modal').removeClass('visible');
$('.youtube-video-container').removeClass('opacity-1');
$('#outer_FP').removeClass('visible');
});


$("#li_menu_about").click(function(e){
$("#container_explore").removeClass('show_aboutus');
if($("#container_aboutus").hasClass('show_aboutus')){
$("#container_aboutus").removeClass('show_aboutus');
}else{
$("#container_aboutus").addClass('show_aboutus');
}
e.stopPropagation();
})
$(document).click(function(evt){
$("#container_aboutus").removeClass('show_aboutus');
});


$("#li_menu_more").click(function(e){
console.log('done');
$("#container_aboutus").removeClass('show_aboutus');
if($("#container_explore").hasClass('show_aboutus')){
$("#container_explore").removeClass('show_aboutus');
}else{
$("#container_explore").addClass('show_aboutus');
}
e.stopPropagation();
})
$(document).click(function(evt){

});


var dropdown_element = document.getElementById('dropdown_container');
//var dropdown_container1 = new Dropdown(dropdown_element,'dropdown_container','dropdown_container_placeholder');
var dropdown_element2 = document.getElementById('name_dropdown_container');
//var dropdown_container2 = new Dropdown(dropdown_element2,'name_dropdown_container','name_dropdown_placeholder');


function show_dropdown_container_list(){
$('#dropdown_container .dropdown_child').addClass('show_dropdown_child');
}


function show_name_dropdown_list(){
$('#name_dropdown_container .dropdown_child').addClass('show_dropdown_child');
}

function hide_dropdown_container_list(){
$('#dropdown_container .dropdown_child').removeClass('show_dropdown_child');
}


function hide_name_dropdown_list(){
$('#name_dropdown_container .dropdown_child').removeClass('show_dropdown_child');
}


function show_location_dropdown_container_list(){
$('#location_dropdown_container .dropdown_child').addClass('show_dropdown_child');
}


function hide_location_dropdown_container_list(){
$('#location_dropdown_container .dropdown_child').removeClass('show_dropdown_child');
}


function show_category_dropdown_container_list(){
$('#category_dropdown_container .dropdown_child').addClass('show_dropdown_child');
}


function hide_category_dropdown_container_list(){
$('#category_dropdown_container .dropdown_child').removeClass('show_dropdown_child');
}


$(document).bind('click',function(){
hide_dropdown_container_list();
hide_name_dropdown_list();
hide_location_dropdown_container_list();
hide_category_dropdown_container_list();
});


$("#filter_dropdown_child .dropdown_option").bind('click',function(e){
e.stopPropagation();
});


$('#dropdown_container_placeholder').bind('click',function(e){
e.preventDefault();
e.stopPropagation();
if($('#dropdown_container .dropdown_child').hasClass('show_dropdown_child')){
hide_dropdown_container_list();
}else{
var text = $(this).text();
console.log(text);

show_dropdown_container_list();
}

});


$('#dropdown_container li.dropdown_option').bind('click',function(e){
e.preventDefault();
e.stopPropagation();
var text = $(this).text();
console.log(text);

$("#dropdown_container_placeholder").text(text);
hide_dropdown_container_list();

});

$('#name_dropdown_container li.dropdown_option').bind('click',function(e){
e.preventDefault();
e.stopPropagation();
var text = $(this).text();
console.log(text);

$("#name_dropdown_placeholder").text(text);
hide_name_dropdown_list();

});


$("#name_dropdown_placeholder").bind('click',function(e){
e.preventDefault();
e.stopPropagation();
if($('#name_dropdown_container .dropdown_child').hasClass('show_dropdown_child')){
hide_name_dropdown_list();
}else{
var text = $(this).text();
console.log(text);

show_name_dropdown_list();
}

});




$('#name_dropdown_container li.dropdown_option').bind('click',function(e){
e.preventDefault();
e.stopPropagation();
var text = $(this).text();
console.log(text);

$("#name_dropdown_placeholder").text(text);
hide_name_dropdown_list();

});


$("#location_dropdown_container_placeholder").bind('click',function(e){
e.preventDefault();
e.stopPropagation();
console.log('done');
if($('#location_dropdown_container .dropdown_child').hasClass('show_dropdown_child')){
hide_location_dropdown_container_list();
}else{
var text = $(this).text();
console.log(text);

show_location_dropdown_container_list();
}

});



$("#category_dropdown_container_placeholder").bind('click',function(e){
e.preventDefault();
e.stopPropagation();
console.log('done');
if($('#category_dropdown_container .dropdown_child').hasClass('show_dropdown_child')){
hide_category_dropdown_container_list();
}else{
var text = $(this).text();
console.log(text);

show_category_dropdown_container_list();
}

});




});


</script>
</head>
<body>
<div class="youtube-video-container">
		<iframe width="560" height="315" src="//www.youtube.com/embed/jSPTF-WBkqw" frameborder="0" allowfullscreen></iframe>
	</div>
    <div id="container_aboutus" class="container_aboutus">  <!--submenu for aboutus-->
	    <p class="p_aboutus">In 1981, seven engineers started Infosys Limited with just US$250. From the beginning, the company was founded on the principle of building and implementing great ideas that drive progress for clients and enhance lives through enterprise solutions. For over three decades, we have been a company focused on bringing to life great ideas and enterprise solutions that drive progress for our clients.</p>
	</div>
	
	
	 <div id="container_explore" class="container_explore"> <!--submenu for explore more-->
     <div id="container_explore1">
	       <div class="explore_item">
		     <div class="explore_heading">
			    <p class="p20">Most Popular</p>
			 </div>
            <div class="explore_content">			 
			  <ul>
			     <li class="explore_li"><a href="#">Pizza Hut</a></li>
			     <li class="explore_li"><a href="#">Subway</a></li>
			     <li class="explore_li"><a href="#">Mast kalander</a></li>
			     <li class="explore_li"><a href="#">Baskin Robbins</a></li>
			     <li class="explore_li"><a href="#">Yo China</a></li>
			     <li class="explore_li"><a href="#">Pind Bolluchi</a></li>
			  </ul>
			 </div> 
		   </div>
		   
		   <div class="explore_item">
		      <div class="explore_heading">
			    <p class="p20">How it works</p>
			 </div>
             <div class="explore_content">
			     <div>
					<iframe class="explore_video" width="100" height="100" src="//www.youtube.com/embed/jSPTF-WBkqw" frameborder="0" allowfullscreen></iframe>
				 </div>
				 <div><a class="explore_watch" href="#">Watch >></a></div>
			 </div>			 
		   </div>
		   
		   <div class="explore_item">
		      <div class="explore_heading">
			    <p class="p20">Let's Be Friends</p>
			 </div>
			 <div class="explore_content">
			 <div class="explore_social">
			    <ul>
				    <li class="e_social_icon"><a href="#"><img src="img/facebook.png"/></a></li>
				    <li class="e_social_icon"><a href="#"><img src="img/twitter.png"/></a></li>
				    <li class="e_social_icon"><a href="#"><img src="img/instagram.png"/></a></li>
				</ul>
			 </div>
			 <div class="explore_social_name">
			       <li class="e_social_name"><a target="_ " href="https://www.facebook.com/pages/Foodylite/1457529654520045" >Facebook</a></li>
			      <li class="e_social_name"><a href="#">Twitter</a></li>
			      <li class="e_social_name"><a href="#">Instagram</a></li>
			 </div>
			 </div>
		   </div>

		   <div class="explore_item">
		      <div class="explore_heading">
			    <p class="p20">Let's Be Friends</p>
			  </div>
			  <div class="explore_content">
			     <p class="p21">Email: chirag@foodylite.com</p>
			     <p class="p21">Phone:9999999999</p>
			  </div>
		  </div> 
		   
	   </div>
	</div> <!--end of submenu-->
	
<?php
require_once('header.php');

?>

<div id="main-container">
    <div id="container1">
	
        <div id="container2">
        <div class="container4_search">
        <div class="dropdown_container" id="dropdown_container">
<div class="dropdown_placeholder" id="dropdown_container_placeholder">
Gurgaon
</div>
<div class="dropdown_child">
<ul>
<li class="dropdown_option">Gurgaon</li>
<!--<li class="dropdown_option">All</li>-->
</ul>
</div>
</div>


<input type="text" name="search_shop_container" placeholder="SEARCH BY SHOP NAME" class="search_shop_container container_search_input" />

  
        <button class="container4_search_button" style="background:rgb(246,201,14);
border:none;
margin-left:9px;
font-family:'Avenir LT 35 Light';height:42px;width:42px;line-height:42px;cursor:pointer;font-size:14px;">GO</button>
        </div>
		   <div class="container2_dropdown">
		   </div>
		   <p class="p1">Search for Confectioneries and Bakers in Delhi/NCR</p>
		</div>
		
		<div id="container3">
		  <!-- <div class="container3_p">
		     <p class="p2">FILTER SHOP:</p>
		   </div>
		   <div class="container3_btn">
		   </div>
		   <div class="container3_btn">
		   </div>  -->
		</div>
		
		<div id="container4">
		  <p class="p3">Confectioneries and bakers in Delhi/NCR</p>
		  <div class="border_1"></div>
		  <div class="border_2"></div>
		</div>
		
		
		<div class="filter_shops_container">
		<div class="filter_label_container"><label>FILTER SHOP: </label>
		</div>
		        <div class="dropdown_container" id="location_dropdown_container">
<div class="dropdown_placeholder" id="location_dropdown_container_placeholder">
LOCATION:
</div>
<div class="dropdown_child" id="filter_dropdown_child">
<ul>
<li class="dropdown_option">Gurgaon</li>
<!--<li class="dropdown_option">All</li>-->
</ul>
</div>
</div>
        <div class="dropdown_container" id="category_dropdown_container">
<div class="dropdown_placeholder" id="category_dropdown_container_placeholder">
CATEGORY
</div>
<div class="dropdown_child" id="filter_dropdown_child">
<ul>
<li class="dropdown_option"><input type="checkbox" name="sweets_checkbox" class="sweets_checkbox" id="sweets_checkbox" /> 
<label for="sweets_checkbox" id="sweets_label">Sweets</label></li>
<li class="dropdown_option"><input type="checkbox" name="cupcake_checkbox" class="cupcake_checkbox" id="cupcake_checkbox" /> <label for="cupcake_checkbox" id="cupcake_label">Cupcake</label></li>
<!--<li class="dropdown_option">All</li>-->
</ul>
</div>
</div>

<div class="dropdown_clear">

</div>
		</div>
		
		<div id="container5"> <!--Container5-->
		  
		  <?php
		  
		  foreach($shops as $sh){
		  echo 
		  
		  '<div class="container5_product">
		     <div class="container5_img">
			    <img src="img/shop1.png"/>
			 </div>
			 <div class="container5_desc">
			    <div class="container5_name">
				   <p class="p4">'.$sh['name'].'</p>
				   <p class="p5">'.$sh['street_address'].'</p>
				   <p class="p6">Opening hours: '.$sh['opening_hours'].'AM to '.$sh['closing_hours'].'PM </p>
				</div>
				<div class="container5_btn">
				  <a id="btn1_1" href="http://amituslab.com/magento4/custom/product_list.php?shop_id='.$sh['id'].'">VISIT STORE</a>
				</div>
			 </div>
		  </div>';
		  }
		  ?>
		  <!--

           <div class="container5_product">
		     <div class="container5_img">
			   <img src="img/shop1.png"/>
			 </div>
			 <div class="container5_desc">
			    <div class="container5_name">
				   <p class="p4">Anand bhavan</p>
				   <p class="p5">Kormangla, Banglore.</p>
				   <p class="p6">Opening hours: Mon-Sat 11:00 AM - 9:00 PM </p>
				</div>
				<div class="container5_btn">
				  <button id="btn1_1"><a href="#">VISIT STORE</a></button>
				</div>
			 </div>
		  </div>
		  
		  <div class="container5_product">
		     <div class="container5_img">
			   <img src="img/shop1.png"/>
			 </div>
			 <div class="container5_desc">
			    <div class="container5_name">
				   <p class="p4">Anand bhavan</p>
				   <p class="p5">Kormangla, Banglore.</p>
				   <p class="p6">Opening hours: Mon-Sat 11:00 AM - 9:00 PM </p>
				</div>
				<div class="container5_btn">
				  <button id="btn1_1"><a href="#">VISIT STORE</a></button>
				</div>
			 </div>
		  </div>
		  
		  <div class="container5_product">
		     <div class="container5_img">
			   <img src="img/shop1.png"/>
			 </div>
			 <div class="container5_desc">
			    <div class="container5_name">
				   <p class="p4">Anand bhavan</p>
				   <p class="p5">Kormangla, Banglore.</p>
				   <p class="p6">Opening hours: Mon-Sat 11:00 AM - 9:00 PM </p>
				</div>
				<div class="container5_btn">
				  <button id="btn1_1"><a href="#">VISIT STORE</a></button>
				</div>
			 </div>
		  </div>
		  
		  <div class="container5_product">
		     <div class="container5_img">
			    <img src="img/shop1.png"/>
			 </div>
			 <div class="container5_desc">
			    <div class="container5_name">
				   <p class="p4">Anand bhavan</p>
				   <p class="p5">Kormangla, Banglore.</p>
				   <p class="p6">Opening hours: Mon-Sat 11:00 AM - 9:00 PM </p>
				</div>
				<div class="container5_btn">
				  <button id="btn1_1"><a href="#">VISIT STORE</a></button>
				</div>
			 </div>
		  </div>

		  <div class="container5_product">
		     <div class="container5_img">
			  <img src="img/shop1.png"/>
			 </div>
			 <div class="container5_desc">
			    <div class="container5_name">
				   <p class="p4">Anand bhavan</p>
				   <p class="p5">Kormangla, Banglore.</p>
				   <p class="p6">Opening hours: Mon-Sat 11:00 AM - 9:00 PM </p>
				</div>
				<div class="container5_btn">
				  <button id="btn1_1"><a href="#">VISIT STORE</a></button>
				</div>
			 </div>
		  </div>

		  <div class="container5_product">
		     <div class="container5_img">
			    <img src="img/shop1.png"/>
			 </div>
			 <div class="container5_desc">
			    <div class="container5_name">
				   <p class="p4">Anand bhavan</p>
				   <p class="p5">Kormangla, Banglore.</p>
				   <p class="p6">Opening hours: Mon-Sat 11:00 AM - 9:00 PM </p>
				</div>
				<div class="container5_btn">
				  <button id="btn1_1"><a href="#">VISIT STORE</a></button>
				</div>
			 </div>
		  </div>
		  
		  -->
		</div> <!--End of container5-->		
    </div>
</div>
 <footer>  <!--Footer-->
	    <div id="footer_footer">
	      <div class="footer_container">
		    <div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Popular Restaurants</p>
			  </div>	
			   <ul>
			      <li class="footer_li"><a href="#">Pizza Hut</a></li>
			      <li class="footer_li"><a href="#">Subway</a></li>
			      <li class="footer_li"><a href="#">Mast Kalander</a></li>
			      <li class="footer_li"><a href="#">Bascin Robbins</a></li>
			      <li class="footer_li"><a href="#">Yo China</a></li>
			      <li class="footer_li"><a href="#">Pind Bolluchi</a></li>
			      <li class="footer_li"><a href="#">Dana Choga</a></li>
			      <li class="footer_li"><a href="#">Mamagoto</a></li>
			      <li class="footer_li"><a href="#">Chin Chin</a></li>
			      <li class="footer_li"><a href="#">Arabesque</a></li>
				  
			   </ul>
			</div>
			
			<div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Popular Cuisines</p>
			  </div>	
			   <ul>
			      <li class="footer_li"><a href="#">Bengali</a></li>
			      <li class="footer_li"><a href="#">Biryani</a></li>
			      <li class="footer_li"><a href="#">Cakes-Bakery</a></li>
			      <li class="footer_li"><a href="#">Chinese</a></li>
			      <li class="footer_li"><a href="#">Continental</a></li>
			      <li class="footer_li"><a href="#">Fast Food</a></li>
			      <li class="footer_li"><a href="#">Hyderabadi</a></li>
			      <li class="footer_li"><a href="#">Ice creams</a></li>
			      <li class="footer_li"><a href="#">Italian</a></li>
			      <li class="footer_li"><a href="#">Japanese</a></li>
				  
			   </ul>
			</div>
			
			<div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Foodylite</p>
			  </div>	
			   <ul>
			      <li class="footer_li"><a href="#">About us</a></li>
			      <li class="footer_li"><a href="#">foodylite.in press</a></li>
			      <li class="footer_li"><a href="#">Contact</a></li>
			      <li class="footer_li"><a href="#">Terms and Conditions</a></li>
			      <li class="footer_li"><a href="#">Privacy Policy</a></li>
				  
			   </ul>
			</div>
			
			<div class="container6">
			  <div class="container6_heading">
			    <p class="p11">Be a contributor</p>
			  </div>	
			  <p class="p12">If you wish to share your recipes through our blog, please leave your email:</p>
			  <div class="input1">
			    <input class="input2" type="text" placeholder="Enter your Email id"/>
			  </div>
			  <div class="submit">
			    <button id="btn1_btn3"><a href="#">SUBMIT</a></button>
			  </div>
			</div>
		 </div>
		 <div id="footer_last">
		    <div class="last_item">
			   <p class="p13">&copy foodylite 2014</p>
			</div>
			
			<div class="last_item">
			   <p class="p14">We currently deliver in Delhi/NCR</p>
			</div>
			
			<div class="last_item" id="social">
			   <ul>
			      <li class="social"><a href="#"><img src="img/facebook.png"/></a></li>
				  <li class="social"><a href="#"><img src="img/twitter.png"/></a></li>
				  <li class="social"><a href="#"><img src="img/instagram.png"/></a></li>
			   </ul>
			</div>
			
		</div>
	  </div>
	</footer>  <!--End of footer-->
	
<!--SIGN In-->
   <div id="container_SI" class="lightbox_options_modal signin_modal">
        <div id="login_img">
		   <img src="img/login.png"/>
		</div>
		
		<div id="login_form">
		   <div class="login_form_left">
		      <div id="login_buttons">
			     <div class="button_google">
				    <button id="btn2_1"><a href="#">Sign in with Google</a></button>
				 </div>
				 <div class="border_SI_3"></div>
				 
				 <div class="button_google">
				    <button id="btn2_2"><a href="#">Login with Facebook</a></button>
				 </div>
				 <div class="border_SI_4"></div>
			  </div>
		   </div>
		   <div class="login_form_middle">
		      <p class="pp1">OR</p>
		   </div>
		   
		   <div class="border_SI_1">
		   </div>
		   
		   <div class="border_SI_2">
		   
		   </div>
		   
		   <div class="login_form_left1">
		       <div id="form_input">
		       <form action="<?php echo $SERVER['PHP_SELF']; ?>" method="post">
			      <div class="form_input1">
				     <input type="text" name="login_username" class="text1" placeholder="Your Email"/>
				  </div>
				  <div class="form_input1">
				     <input type="password" name="login_password" class="text1" placeholder="Password"/>
				  </div>
				  
			   </div>
		   </div>
		</div>
		
		<div id="button_log">
		    <div class="log_forgot">
			   <input type="submit" id="btn2_3" value="LOG IN" />
			</div>
			</form>
			<div class="log_forgot1">
			   <p class="pp2"><a href="#">Forgot your Password?</a></p>
			</div>
		</div>
	     
        <div id="login_account">
		   <p class="pp3">Don't have an Account ?</p>
		   <p class="pp4"><a href="#">Sign Up</a></p>
		</div>		 
	 </div> 
	 <!--End of SignIn--> 
	 
	  <!--FORGOT YOUR PASSWORD-->
	<div id ="outer_FP">
		   <div id="inner_FP">
		      <div class="FP_container1">
			     <img src="img/find-pass.png"/>
			  </div>
              <div class="FP_container2">
			     <input class="FP_input" placeholder="Your Email"/>
			  </div>
              <div class="FP_container3">
			     <button class="FP_submit"><a href="javascript:void(0)">Submit</a></button>
			  </div>
              <p class="FP_p1">To retrieve your password, enter the email address</p>			  
		   </div>
        </div>
		
		<!--FORGOT YOUR PASSWORD-->

   <!--Sign Up-->
 <div id="outer_signup" class="lightbox_options_modal signup_modal">
   <div id="main-container_SU">
       <div id="container_SU">
	        <div id="container1_SU">
			  <div class="container1_button">
			     <button id="btn3_1"><a href="#">Signup with Facebook</a></button>
			  </div>
			  <div class="container1_p">
			     <p class="ppp1">We will never post anything on your wall. Promise! </p>
			  </div>
			</div>
			
			<div id="container2_SU">
			   <p class="ppp2">OR</p>
			</div>
			
			<div class="border_border">
			</div>
			<div class="border_border1">
			</div>
			
			<div class="border_border2">
			</div>
			
			<div id="container3_SU">
			   
			   <div class="container3_input">
			       <div class="input_name1">
				      <input type="text" class="name" placeholder="First Name"/>
				   </div>
				   
				   <div class="input_name2">
				      <input type="text" class="name" placeholder="Last Name"/>
				   </div>
			   </div>
			   
			   <div class="container3_input">
			     <div class="input_name">
				      <input type="text" class="input_email" placeholder="Email"/>
				   </div>
			   </div>
			   
			   <div class="container3_input">
			       <div class="input_name">
				      <input type="text" class="input_email input_passwd" placeholder="Password( Minimum 8 Characters)"/>
				   </div>
			   </div>
			   
			   <div class="container3_input">
			     <div class="input_name">
				      <input type="text" class="input_email" placeholder="Confirm Password"/>
				   </div>
			   </div>
			   
			   <div class="container3_input">
			     <div class="input_name3">
				      <input type="text" class="name3" placeholder=""/>
					  <div class="fixed_contact">
					     +91
					  </div>
				   </div>
				   
				   <div class="input_name4">
				      <input type="text" class="name" placeholder="Zip Code"/>
				   </div>
			   </div>
			</div>
			
			<div id="container4_SU">
			   <div class="container4_all">
			     <button id="btn3_btn2"><a href="#">SIGN UP</a></button>
			   </div>
			   
			   <div class="container4_all all1">
			     <p class="ppp4">Already have an Account ?<span class="ppp5"><a href="#">Log In</a></span></p>
			   </div>
			   
			   <div class="container4_all">
			     <p class="ppp6">By signing up, you agree to the privacy policy and terms and conditions</p>
			   </div>
			   
			</div>
	   </div>
   </div>  
   </div> 
   
   <div class="home_lightbox">
   
   </div>
   
   
   <script type="text/javascript" src="js/jquery-2.1.1.js"></script>
   <script type="text/javascript" src="js/search.js"></script>
   <script type="text/javascript" src="js/Dropdown.js"></script>

</body>
</html>
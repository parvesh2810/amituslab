jQuery(document).ready(function() {
    /* for set top banner session */
    jQuery('#header-banners-close').on('click', function(){
        var data_send = 'action=fw_set_top_banner_session&ajax=true';
        /* send data with ajax */
        jQuery.ajax({
            type: 'POST',
            url: FhPhpVar.ajaxurl,
            data: data_send,
            success: function(response)
            {
                console.log('success');
            },
            error : function(response){
                console.log('error');
            }
        });
    });
    jQuery('.inputs-section').removeClass('invisible');
    var $ = jQuery;
    var screenRes = $(window).width(),
        screenHeight = $(window).height(),
        html = $('html');

// IE<8 Warning
    if (html.hasClass("oldie")) {
        $("body").empty().html('Please, Update your Browser to at least IE8');
    }

// Remove outline in IE
    $("a, input, textarea").attr("hideFocus", "true").css("outline", "none");

// Main Slider Vertical centered controls
    var msControlsHeight = $('.main-slider .carousel-indicators').innerHeight();
    $('.main-slider .carousel-indicators').css('margin-top' , -msControlsHeight/2);

// styled Select, Radio, Checkbox
    if ($(".select-styled").length) {
        cuSel({changedEl: ".select-styled", visRows: 7});
    }

    /*    if ($("div, p").hasClass("input-styled")) {
     $(".input-styled input").customInput();
     }*/

// Tabs with Doubled Bullets
    $('.tabs').on('click', '[data-toggle="tab"]', function() {
        var target = $(this).attr('href');
        $('a[href="'+target+'"]').tab('show');
    });

// prettyPhoto lightbox, check if <a> has atrr data-rel and hide for Mobiles
    if($('a').is('[data-rel]') && screenRes > 600) {
        $('a[data-rel]').each(function() {
            $(this).attr('rel', $(this).data('rel'));
        });
        $("a[rel^='prettyPhoto']").prettyPhoto({theme: 'dark_square', social_tools:false, horizontal_padding: 80});
    }


    // Banners on Top
    $('#header-banners-close').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.header-banners').slideUp(300);
    });

    //Scroll To Top
    $('.anchor[href^="#"]').on('click', function(e) {
        e.preventDefault();
        var speed = 2,
            boost = 1,
            offset = 80,
            target = $(this).attr('href'),
            currPos = parseInt($(window).scrollTop(), 10),
            targetPos = target!="#" && $(target).length==1 ? parseInt($(target).offset().top, 10)-offset : currPos,
            distance = targetPos-currPos,
            boost2 = Math.abs(distance*boost/1000);
        $("html, body").animate({ scrollTop: targetPos }, parseInt(Math.abs(distance/(speed+boost2)), 10));

    });
    // Back To Top Button
    $(window).on('scroll', function() {
        if(parseInt($(window).scrollTop(), 10) > 600){
            $('#toTop').fadeIn(500);
        }
        else {
            $('#toTop').fadeOut(500);
        }
    });


    /* for menu effects */
    jQuery('.nav-menu').slicknav();

    jQuery(".nav-menu ul").addClass('animated hidden');

    var menuItemWidth, submenuItemWidth;

    jQuery(".nav-menu > li").hover(function(){
        var $this = jQuery(this);

        $this.children('ul').removeClass().addClass('animated');
        menuItemWidth = $this.innerWidth();
        submenuItemWidth = $this.children("ul").innerWidth();
        $this.children("ul").css('left' , (menuItemWidth-submenuItemWidth)/2);

        $this.children('ul').addClass('fadeInDownSmall').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $this.children('ul').removeClass().addClass('animated')
        });
    }, function(){
        jQuery(this).children('ul').addClass('fadeOutUpSmall').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            jQuery(this).removeClass().addClass('hidden');
        });
    });

    <!-- ContactUs Modal Window -->
    $('[href="#contact-us"]').on('click', function(e) {
        e.preventDefault();
        $('.section-contact-popup .page-overlay, .section-contact-popup .contact-modal').removeClass('hidden');
        window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return false;};
        $('#contact-note, #contact-error').hide();
        $('#contact-form-container').show();
    });

    $('.section-contact-popup').on('click', '.page-overlay, .icon-close-thin', function() {
        var overlay = $(this).parents('.section-contact-popup').children('.page-overlay'),
            modal = $(this).parents('.section-contact-popup').children('.contact-modal');

        if (Modernizr.cssanimations) {
            modal.addClass('growOut');
            overlay.addClass('fadeOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                overlay.removeClass('fadeOut').addClass('hidden');
                modal.removeClass('growOut').addClass('hidden');
                window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
            });
        } else {
            overlay.addClass('hidden');
            modal.addClass('hidden');
            window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
        }
    });

    $('#contact-message').on('focus', function() {
        var tm = $('#tm').val();
        $('#contactform').append('<input type="hidden" name="token_m" value="'+tm+'" />')
    });
    $('#contact-email').on('focus', function() {
        var te = $('#te').val();
        $('#contactform').append('<input type="hidden" name="token_e" value="'+te+'" />')
    });

    $('#contactform').on('submit', function(e){
        e.preventDefault();
        var contact_error = false;

        $('#contact-author, #contact-email, #contact-message').each(function() {
            var element = $(this).parent();
            var value = $(this).val();
            element.removeClass('error valid');
            if (value == '') {
                element.addClass('error');
                contact_error = true;
            } else {
                element.addClass('valid');
            }
        });

        if (contact_error) return;

        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            if(data != null && data.msg == 'Message sent'){
                $('#contact-note').show();
                $('#contact-form-container').hide();
            } else {
                $('#contact-error').show();
                $('#contact-form-container').hide();
            }
            $('#contact-author, #contact-email, #contact-message').val('');
            $("input[name='token_m'], input[name='token_e']").remove();
        }, 'json');
    });

    <!-- Send Link Modal Window -->
    $('[href="#send-link"]').on('click', function(e) {
        e.preventDefault();
        $('.send-link .page-overlay, .send-link .contact-modal').removeClass('hidden');
        window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return false;};
        $('#send-link-note, #send-link-error').hide();
        $('#send-link-form-container').show();
    });

    $('.send-link').on('click', '.page-overlay, .icon-close-thin', function() {
        var overlay = $(this).parents('.send-link').children('.page-overlay'),
            modal = $(this).parents('.send-link').children('.contact-modal');

        if (Modernizr.cssanimations) {
            modal.addClass('growOut');
            overlay.addClass('fadeOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                overlay.removeClass('fadeOut').addClass('hidden');
                modal.removeClass('growOut').addClass('hidden');
                window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
            });
        } else {
            overlay.addClass('hidden');
            modal.addClass('hidden');
            window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
        }
    });

    $('#site-name').on('focus', function() {
        var tm = $('#tm').val();
        $('#send-link').append('<input type="hidden" name="token_m" value="'+tm+'" />')
    });
    $('#site-link').on('focus', function() {
        var te = $('#te').val();
        $('#send-link').append('<input type="hidden" name="token_e" value="'+te+'" />')
    });

    $('#send-link').on('submit', function(e){
        e.preventDefault();
        var send_link_error = false;

        $('#site-name, #site-link, #select-theme').each(function() {
            var element = $(this).parents('div[class^=field]');
            var value = $(this).val();

            element.removeClass('error valid');
            baseclases = element.attr('class');
            if (value == '' || value == 0) {
                element.attr('class', baseclases).addClass('error');
                send_link_error = true;
            } else {
                element.attr('class', baseclases).addClass('valid');
            }
        });

        if (send_link_error) return;

        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            if(data != null && data.msg == 'Message sent'){
                $('#send-link-note').show();
                $('#send-link-form-container').hide();
            } else {
                $('#send-link-error').show();
                $('#send-link-form-container').hide();
            }
            $('#site-name, #site-link, #select-theme').val('');
            $("input[name='token_m'], input[name='token_e']").remove();
        }, 'json');
    });

    <!-- Affiliates Modal Window -->
    $('[href="#join-affiliate"]').on('click', function(e) {
        e.preventDefault();
        $('.section-join-affiliate .page-overlay, .section-join-affiliate .contact-modal').removeClass('hidden');
        window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return false;};
        $('#affiliate-note, #affiliate-error').hide();
        $('#affiliate-form-container').show();
    });

    $('.section-join-affiliate').on('click', '.page-overlay, .icon-close-thin', function() {
        var overlay = $(this).parents('.section-join-affiliate').children('.page-overlay'),
            modal = $(this).parents('.section-join-affiliate').children('.contact-modal');

        if (Modernizr.cssanimations) {
            modal.addClass('growOut');
            overlay.addClass('fadeOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                overlay.removeClass('fadeOut').addClass('hidden');
                modal.removeClass('growOut').addClass('hidden');
                window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
            });
        } else {
            overlay.addClass('hidden');
            modal.addClass('hidden');
            window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
        }
    });

    $('#affiliate-site').on('focus', function() {
        var tm = $('#tm').val();
        $('#affiliateform').append('<input type="hidden" name="token_m" value="'+tm+'" />')
    });
    $('#affiliate-email').on('focus', function() {
        var te = $('#te').val();
        $('#affiliateform').append('<input type="hidden" name="token_e" value="'+te+'" />')
    });

    $('#affiliateform').on('submit', function(e){
        e.preventDefault();
        var affiliate_error = false;

        $('#affiliate-site, #affiliate-email, #affiliate-name').each(function() {
            var element = $(this).parent();
            var value = $(this).val();
            element.removeClass('error valid');
            if (value == '') {
                element.addClass('error');
                affiliate_error = true;
            } else {
                element.addClass('valid');
            }
        });

        if (affiliate_error) return;

        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            if(data != null && data.msg == 'Message sent'){
                $('#affiliate-note').show();
                $('#affiliate-form-container').hide();
            } else {
                $('#affiliate-error').show();
                $('#affiliate-form-container').hide();
            }
            $('#affiliate-site, #affiliate-email, #affiliate-name, #affiliate-message').val('');
            $("input[name='token_m'], input[name='token_e']").remove();
        }, 'json');
    });


    /* tooltip */
    $("[class^='location-tooltip-']").addClass('animated');

    $(".info-contacts-right a").hover(function(){
        $(this).children("[class^='location-tooltip-']").removeClass('hidden').addClass('animated fadeInDownSmall').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass('fadeInDownSmall')
        });

        var tooltip = $(this).children("[class^='location-tooltip-']"),
            ttWidth = tooltip.outerWidth(),
            ttoffset = parseInt(tooltip.offset().left, 10),
            screenWidth = $(window).width();

        if (ttoffset+ttWidth > screenWidth) {
            tooltip.css('left' , -(ttWidth/2+ttoffset+ttWidth-screenWidth))
        } else {
            tooltip.css('left' , -170)
        }

    }, function(){
        $(this).children("[class^='location-tooltip-']").addClass('hidden').css('left' , -170);
    });

    $('.info-contacts-right').on('click', 'a', function(e) {
        e.preventDefault();
    });

    /* placeholder */
    if(jQuery("[placeholder]").size() > 0) {
        jQuery.Placeholder.init({ color : "#0c9" });
    }

    /* for view theme showcase */
    var theme_showcase_url = window.location.href.split('#theme=')[1];
    if(theme_showcase_url!=undefined){
        var filter = jQuery('#categories-slider');
        filter.find('.categories-item.active').removeClass('active');
        filter.find('.categories-item a[href="#theme='+theme_showcase_url+'"]').parent('li').addClass('active');
        jQuery('#cuselFrame-theme-filter .cusel-scroll-wrap span.cuselActive').removeClass('cuselActive');
        var active_item_label = jQuery('#cuselFrame-theme-filter .cusel-scroll-wrap span[val="'+theme_showcase_url+'"]').addClass('cuselActive').html();
        jQuery('#cuselFrame-theme-filter .cuselText').html(active_item_label);

        /* for filter current theme */
        if(theme_showcase_url!='all')
            var option = theme_showcase_url;
        else
            var option = '';
        search = option ? function() {
            var $item = jQuery(this),
                name = $item.data('category') ? $item.data('category') : '';
            return name.match(new RegExp(option));
        } : '*';
        jQuery('#themelist').isotope({filter : search});
    }

    /* for blog details */
    jQuery('.anchor[href^="#"]').on('click', function(e) {
        e.preventDefault();
        var speed = 2,
            boost = 1,
            offset = 80,
            target = $(this).attr('href'),
            currPos = parseInt($(window).scrollTop(), 10),
            targetPos = target!="#" && $(target).length==1 ? parseInt($(target).offset().top, 10)-offset : currPos,
            distance = targetPos-currPos,
            boost2 = Math.abs(distance*boost/1000);
        jQuery("html, body").animate({ scrollTop: targetPos }, parseInt(Math.abs(distance/(speed+boost2)), 10));

    });

    /* start slider with theme-categories */
    if(jQuery('#categories-slider').length > 0){
        function catInit() {
            jQuery('#categories-slider').carouFredSel({
                swipe : {
                    onTouch: true
                },
                prev: '#categories-prev',
                next: '#categories-next',
                items: {visible: "variable"},
                auto: {
                    play: true,
                    timeoutDuration: 10000
                },
                scroll: {
                    pauseOnHover: true,
                    items: 1,
                    duration: 1000,
                    easing: 'quadratic'
                }
            })
        }
        catInit();
        jQuery(window).resize(function() {
            catInit();
        });
    }

    /* start slider latest-post */
    if(jQuery('#latest-post').length > 0){
        function latestInit() {
            jQuery('#latest-post').carouFredSel({
                swipe : {
                    onTouch: true
                },
                next : "#latest-post-next",
                prev : "#latest-post-prev",
                infinite: true,
                width: "100%",
                responsive: true,
                items: 1,
                auto: {
                    play: true,
                    timeoutDuration: 10000
                },
                scroll: {
                    items : 1,
                    fx: "crossfade",
                    easing: "linear",
                    pauseOnHover: true,
                    duration: 300
                }
            });
        }
        if(jQuery('#latest-post').length!=0) latestInit();
        jQuery(window).resize(function() {
            if(jQuery('#latest-post').length!=0) latestInit();
        });
    }

    /* start_testimonials slider */
    if(jQuery('#testimonials').length > 0){
        function testimonialsInit() {
            jQuery('#testimonials').carouFredSel({
                swipe : {
                    onTouch: true
                },
                next : "#testimonials-next",
                prev : "#testimonials-prev",
                pagination : "#testimonials-controls",
                infinite: false,
                items: 1,
                auto: {
                    play: true,
                    timeoutDuration: 10000
                },
                scroll: {
                    items : 1,
                    fx: "crossfade",
                    easing: "linear",
                    pauseOnHover: true,
                    duration: 300
                }
            });
        }
        testimonialsInit();
        jQuery(window).resize(function() {
            testimonialsInit();
        });
        var tControlsHeight = jQuery('.testimonials-controls').innerHeight();
        jQuery('.testimonials-controls').css('margin-top' , -tControlsHeight/2);
    }

    /* start recent_themes slider */
   function latestInitThemes(e) {
        jQuery(e+'-slider').carouFredSel({
            swipe : {
                onTouch: true
            },
            prev: e+'-prev',
            next: e+"-next",
            circular: false,
            infinite: false,
            auto: false,
            scroll: {
                pauseOnHover: true,
                items: 1,
                duration: 1000,
                easing: 'quadratic'
            }
        });
    } 
   
   var activeTab = jQuery('.latest-themes .tab-labels .active a').attr('href');
    if(activeTab!=undefined) latestInitThemes(activeTab);
    jQuery(window).resize(function() {
        if(activeTab!=undefined) latestInitThemes(activeTab);
    });
   jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        activeTab = jQuery('.latest-themes .tab-labels .active a').attr('href');
        latestInitThemes(jQuery(e.target).attr('href'));
    });

    /* for filter themes */
   jQuery('#themelist').isotope({
        transitionDuration: '0.4s',
        getSortData: {
            name: '[data-name]',
            category: '[data-category]',
            price: '[data-price]',
            popular: '[data-popular]',
            recent: '[data-recent]'
        }
    });

    /* Sorting */
  jQuery('.field-select').on('change', '#theme-sort', function() {
        jQuery('#themelist').isotope( 'remove', jQuery('.banner') );
        var sortData = jQuery(this).val().split('-'),
            option = sortData[0],
            dir = {asc: true, desc: false},
            direction = sortData[1] ? dir[sortData[1]] : dir.asc;

        jQuery('#themelist').isotope({sortBy : option, sortAscending: direction});
    });

    /* Filtering */
  jQuery('.field-select').on('change', '#theme-filter', function() {
        var option = jQuery(this).val(),
            search = option ? function() {
                var $item = jQuery(this),
                    name = $item.data('category') ? $item.data('category') : '';
                return name.match(new RegExp(option));
            } : '*';
        var theme_url = window.location.href.split('#theme=')[0];
        if(option!='')
            window.location.href = theme_url+'#theme='+option;
        else
            window.location.href = theme_url+'#theme=all';
        jQuery('#themelist').isotope({filter : search});
    });
    jQuery('.theme-listing').on('click', '.categories-item', function(e) {
        e.preventDefault();
        jQuery('.categories-item').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('#theme-search').val('');

        var pos = jQuery('#categories-slider').offset(),
            option = jQuery(this).data('category');

        //jQuery("html, body").animate({ scrollTop: (pos.top) }, 400);
        jQuery("html, body").animate({ scrollTop: (pos.top-60) }, 400);
        cuselSetValue("#theme-filter", option);
    }); 

    /* Searching */
  var timeoutId;
    jQuery('.field-text').on('input', '#theme-search', function() {
        clearTimeout(timeoutId);
        var $input = jQuery(this);

        timeoutId = setTimeout(function(){
            var option = $input.val().toLowerCase(),
                search = option ? function() {
                    var $item = jQuery(this),
                        name = $item.data('name') ? $item.data('name') : '';
                    return name.match(new RegExp(option));
                } : '*';

            jQuery('#themelist').isotope({filter : search});
        }, 300);
    });

    /* Nothing Found Message */
  jQuery('#themelist').isotope( 'on', 'layoutComplete', function( isoInstance, laidOutItems ) {
        if (laidOutItems.length > 0) {
            jQuery('.nothing-found').addClass('hidden');
        } else {
            jQuery('.nothing-found').removeClass('hidden');
        }
    } );

    /* Orange Banner Width */
  function bannerWidth () {
        var screenRes = jQuery(window).width(),
            containerRes = jQuery('#themelist').width();
        jQuery('#themelist .banner').css({width: screenRes, marginLeft: (containerRes-screenRes)/2});
    }
    bannerWidth ();
    jQuery(window).resize(function(){
        bannerWidth ()
    });

    /* start theme-details slider */
  if(jQuery('#theme-slider').length > 0){
        function themeInit() {
            jQuery('#theme-slider').carouFredSel({
                prev: '#theme-prev',
                next: '#theme-next',
                items: 1,
                auto: {
                    play: true,
                    timeoutDuration: 5000
                },
                scroll: {
                    items : 1,
                    fx: "crossfade",
                    easing: "linear",
                    pauseOnHover: true,
                    duration: 600
                }
            })
        }
        themeInit();
        jQuery(window).resize(function() {
            themeInit();
        });
    }

    /* start animation for banner slider */
  if(jQuery('#banners-slider').length > 0){
        function bannersInit() {
            jQuery('#banners-slider').carouFredSel({
                prev: '#banners-prev',
                next: '#banners-next',
                auto: {
                    play: true,
                    timeoutDuration: 5000
                },
                scroll: {
                    pauseOnHover: true,
                    items: 1,
                    duration: 1000,
                    easing: 'quadratic'
                }
            })
        }
        bannersInit();
        jQuery(window).resize(function() {
            bannersInit();
        })
    }

    /* for subscribe to newsletter */
  jQuery("#newsletterform").submit(function(event){
        event.preventDefault();
        var surrounding_element = jQuery('#newsletter');
        fh_subscribe_to_news(surrounding_element);
    });

    jQuery("#newsletter").keyup(function(event){
        event.preventDefault();
        if(event.keyCode == 13){
            var surrounding_element = jQuery('#newsletter');
            fh_check_email(surrounding_element);
        }
    });

    function fh_check_email(surrounding_element){
        var value = surrounding_element.attr('value');
        surrounding_element.removeClass('error_email valid_email');
        baseclases = surrounding_element.attr('class');
        if(!value.match(/^\w[\w|\.|\-]+@\w[\w|\.|\-]+\.[a-zA-Z]{2,4}$/)) {
            surrounding_element.attr('class',baseclases).addClass('error_email');
            my_error = true;
        } else {
            surrounding_element.attr('class',baseclases).addClass('valid_email');
            my_error = false;
        }
        return my_error;
    }


    function fh_subscribe_to_news(surrounding_element){
        var my_error = fh_check_email(surrounding_element);
        if(my_error){
            return false;
        }
        else{
            var value = surrounding_element.attr('value');
            var datastring = 'action=fh_subscribe_to_newsletter&subscriber_email=' + value;
            jQuery.ajax({
                type: 'POST',
                url: FhPhpVar.ajaxurl,
                data: datastring,
                success: function(response)
                {
                    if (response == 'true')
                    {
                        jQuery('.newsletter_title span').html('<span class="done_subscription">Done. Thanks for your subscription.</span>');
                    }
                    else
                    {
                        jQuery('.newsletter_title span').html('<span class="sorry_subscription">Sorry, try now to subscribe</span>');
                    }
                    return false;
                }
            });
        }
    }

});

/* for wait header image and sliders animation */

jQuery(function() {
    jQuery('.testimage').load(function(){
        jQuery(".main-header .spinner, .main-header .testimage").remove();
        jQuery(".page-header, .site-logo, .primary-navigation").removeClass('invisible').addClass('animated fadeIn');

        /* for customization-page, hosting-domain and others with this class */
       if(jQuery(".customization-page").length >0){
            setTimeout(function(){
                jQuery(".customization-page .page-title-before").removeClass('invisible').addClass('animated fadeInDown');
                jQuery(".customization-page .page-title").removeClass('invisible').addClass('animated fadeInLeft');
                jQuery(".customization-page .page-subtitle").removeClass('invisible').addClass('animated fadeInRight');
                jQuery(".customization-page .btn").removeClass('invisible').addClass('animated fadeInUp');
            }, 400);
        }

        /* start animation pricing page */
      if(jQuery('.pricing-page').length > 0){
            setTimeout(function(){
                jQuery(".pricing-page .page-title-before").removeClass('invisible').addClass('animated fadeInRight');
                jQuery(".pricing-page .page-title").removeClass('invisible').addClass('animated fadeInLeft');
            }, 200);

            setTimeout(function(){
                jQuery(".pricing-page .pricelist, .section-pricing-buttons ul").removeClass('invisible').addClass('animatedFast fadeInUpSmall');
            }, 600);
        }

        /* start animated for blog */
    if(jQuery('.blog-page').length > 0){
            setTimeout(function(){
                jQuery(".blog-page .page-title-before").removeClass('invisible').addClass('animated fadeInRight');
                jQuery(".blog-page .page-title").removeClass('invisible').addClass('animated fadeInLeft');
            }, 200);
            setTimeout(function(){
                jQuery(".blog-page .categories-slider").removeClass('invisible').addClass('animatedFast fadeInUpSmall');
            }, 600);
        }

        /* start animation for single-post */
   if(jQuery('.blog-details-page').length > 0){
            setTimeout(function(){
                jQuery(".blog-details-page .entry-title").removeClass('invisible').addClass('animated fadeInDownSmall40');
                jQuery(".blog-details-page .entry-meta").removeClass('invisible').addClass('animated fadeInUpSmall');
                jQuery(".blog-details-page a[rel='prev']").css('visibility','visible').addClass('animated fadeInLeft');
                jQuery(".blog-details-page a[rel='next']").css('visibility','visible').addClass('animated fadeInRight');
            }, 200);
        }

        /* start animated for theme-listing */
    if(jQuery('.theme-listing').length > 0){
            setTimeout(function(){
                jQuery(".theme-listing .page-title-before").removeClass('invisible').addClass('animated fadeInRight');
                jQuery(".theme-listing .page-title").removeClass('invisible').addClass('animated fadeInLeft');
            }, 200);
            setTimeout(function(){
                jQuery(".theme-listing .categories-slider").removeClass('invisible').addClass('animatedFast fadeInUpSmall');
            }, 600)
        }

        /* start adnimated for theme-details */
    if(jQuery('.theme-details').length > 0){
            setTimeout(function(){
                jQuery(".theme-details .page-title-before").removeClass('invisible').addClass('animated fadeInRight');
                jQuery(".theme-details .page-title").removeClass('invisible').addClass('animated fadeInLeft');
            }, 400);
            setTimeout(function(){
                jQuery(".theme-details .theme-slider").removeClass('invisible').addClass('animatedFast fadeInUpSmall');
            }, 800);
        }

    });
});

jQuery(function($) {
    $(window).load(function(){
        $("#spinner1, #spinner2").remove();
        $(".latest-slider, .testimonials-slider").removeClass('invisible').addClass('animated fadeIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $(".latest-slider, .testimonials-slider").removeClass('animated fadeIn');
        });
    });
});
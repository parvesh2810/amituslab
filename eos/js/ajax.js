/* Common Module */
$(function common(){
	$('#search-products').bind('keypress',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'sm_products'},success:function(result){
			console.log(result);
		}});
	});
	$('#search-symbols').bind('keypress',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'sm_symbols'},success:function(result){
			console.log(result);
		}});
	});
	$('#search-messages').bind('keypress',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'sm_messages'},success:function(result){
			console.log(result);
		}});
	});
});
/* News Module */
$(function news(){
	$('#world-btn').bind('click',function(){
		$.ajax({url:"php/get_world_news.php",type:"POST",success:function(result){
			console.log(result);
		}});
	});
	$('#system-btn').bind('click',function(){
		$.ajax({url:"php/get_system_news.php",type:"POST",success:function(result){
			console.log(result);
		}});
	});
});

/* B2B Module */

$(function b2b(){
	$('#b2b-new').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'b2b_new'},success:function(result){
			console.log(result);
		}});
	});
	$('#b2b-atoz').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'b2b_atoz'},success:function(result){
			console.log(result);
		}});
	});
	$('#b2b-store').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'b2b_store'},success:function(result){
			console.log(result);
		}});
	});
	$('#b2b-cat').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'b2b_cat'},success:function(result){
			console.log(result);
		}});
	});
	$('#b2b-my').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'b2b_my'},success:function(result){
			console.log(result);
		}});
	});
	$('#req-new').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'req_new'},success:function(result){
			console.log(result);
		}});
	});
	$('#req-atoz').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'req_atoz'},success:function(result){
			console.log(result);
		}});
	});
	$('#req-fact').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'req_fact'},success:function(result){
			console.log(result);
		}});
	});
	$('#req-cat').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'req_cat'},success:function(result){
			console.log(result);
		}});
	});
	$('#req-my').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'req_my'},success:function(result){
			console.log(result);
		}});
	});
	$('#add-req').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'add_req'},success:function(result){
			console.log(result);
		}});
	});
	$('#purc-hist').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'purc_hist'},success:function(result){
			console.log(result);
		}});
	});
	$('#sales-hist').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'sales-hist'},success:function(result){
			console.log(result);
		}});
	});
});

/* Warehouse Module */

$(function warehouse(){
	$('#wh-new').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'wh_new'},success:function(result){
			console.log(result);
		}});
	});
	$('#wh-atoz').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'wh_atoz'},success:function(result){
			console.log(result);
		}});
	});
	$('#wh-store').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'wh_store'},success:function(result){
			console.log(result);
		}});
	});
	$('#wh-cat').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'wh_cat'},success:function(result){
			console.log(result);
		}});
	});
	$('#wh-my').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'wh_my'},success:function(result){
			console.log(result);
		}});
	});
});

/* Stock Module */

$(function stock(){
	$('#stock-home').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'stock_home'},success:function(result){
			console.log(result);
		}});
	});
	$('#stock-new').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'stock_new'},success:function(result){
			console.log(result);
		}});
	});
	$('#stock-atoz').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'stock_atoz'},success:function(result){
			console.log(result);
		}});
	});
	$('#stock-po').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'stock_po'},success:function(result){
			console.log(result);
		}});
	});
	$('#add-order').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'add_order'},success:function(result){
			console.log(result);
		}});
	});
	$('#current-orders').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'current_orders'},success:function(result){
			console.log(result);
		}});
	});
	$('#stock-hist').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'stock_hist'},success:function(result){
			console.log(result);
		}});
	});
	$('#stock-my').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'stock_my'},success:function(result){
			console.log(result);
		}});
	});
});

/* City Module */

$(function city(){
	$('#city-hq').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'city_hq'},success:function(result){
			console.log(result);
		}});
	});
	$('#city-bank').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'city_bank'},success:function(result){
			console.log(result);
		}});
	});
	$('#city-sec').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'city_sec'},success:function(result){
			console.log(result);
		}});
	});
	$('#city-es').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'city_es'},success:function(result){
			console.log(result);
		}});
	});
	$('#city-gov').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'city_gov'},success:function(result){
			console.log(result);
		}});
	});
});

/* Message Module */

$(function messages(){
	$('#msg-received').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'msg_received'},success:function(result){
			console.log(result);
		}});
	});
	$('#msg-sent').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'msg_sent'},success:function(result){
			console.log(result);
		}});
	});
	$('#msg-notes').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'msg_notes'},success:function(result){
			console.log(result);
		}});
	});
	$('#msg-new').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'msg_new'},success:function(result){
			console.log(result);
		}});
	});
	$('#msg-contacts').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'msg_contacts'},success:function(result){
			console.log(result);
		}});
	});
});

/* Ranking Module */

/* EOS-pedia Module */
	
$(function eospedia(){
	$('#info-prod').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'info_prod'},success:function(result){
			console.log(result);
		}});
	});
	$('#info-fact').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'info_fact'},success:function(result){
			console.log(result);
		}});
	});
	$('#info-store').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'info_store'},success:function(result){
			console.log(result);
		}});
	});
	$('#info-cat').bind('click',function(){	
		$.ajax({url:"php/get_system_news.php",type:"POST",data:{action:'info_cat'},success:function(result){
			console.log(result);
		}});
	});
});	
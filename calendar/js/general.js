$(document).ready(function(){	
	$('#calendar').datepicker({  
            inline: true,  
            showOtherMonths: true,  
            dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],  
    });  
	$('tbody tr:nth-child(1) td:last-child').addClass('blue');
	$('tbody tr:nth-child(3) td:nth-child(2)').addClass('green');
	$('tbody tr:nth-child(2) td:nth-child(6)').addClass('orange');
	$('tbody tr:nth-child(4) td:nth-child(4)').addClass('purple');
	$('tbody tr:nth-child(5) td:nth-child(5)').addClass('gray');
});